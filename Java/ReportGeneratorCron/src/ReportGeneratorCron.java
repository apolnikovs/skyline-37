import java.io.File;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import RunUserReport.*;



public class ReportGeneratorCron {
	
	
	
	//Global vars
	public static RunUserReport rur = null;
	
	
	
	//Main method
	@SuppressWarnings("all")
	public static void main(String[] args) throws Exception {
		
		//initialise RunUserReport class
		rur = new RunUserReport();
		
		//retrieve scheduled reports data
		String q = 	"SELECT		user_reports.*, " +
					"			CONCAT(user.ContactFirstName, ' ', user.ContactLastName) AS UserName " +
					"FROM 		user_reports " +
					"LEFT JOIN	user ON user_reports.UserID = user.UserID " +
					"WHERE 		user_reports.Schedule IS NOT NULL AND user_reports.ScheduleStatus = 'on' ";
		ArrayList<Map<String, Object>> result = query(q);
		
		Map<String, Object> schedule = new HashMap<String, Object>();
		Map<String, Object> params = new HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		
		for (Map<String, Object> report: result) {
			
			schedule = mapper.readValue((String) report.get("Schedule"), Map.class);
			params.put("reportID", report.get("UserReportID"));
			params.put("userType", report.get("UserType"));
			params.put("userID", report.get("UserID"));
			params.put("userTypeID", report.get("UserTypeID"));
			params.put("filter", new HashMap<String, Object>());
			params.put("userName", report.get("UserName"));
			params.put("reportName", report.get("Name"));
			params.put("reportDescription", report.get("Description"));
			if (schedule.get("jobType") != null) {
				((Map<String, Object>) params.get("filter")).put("jobType", schedule.get("jobType"));
			}
			((Map<String, Object>) params.get("filter")).put("column", schedule.get("dateType"));
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Calendar dateFrom = Calendar.getInstance();
			Calendar dateTo = Calendar.getInstance();
			Calendar now = Calendar.getInstance();
			
			dateTo.add(Calendar.DATE, -1);
			
			switch ((String)schedule.get("datePeriod")) {
				
				case "daily": {
					
					dateFrom = Calendar.getInstance();
					dateFrom.add(Calendar.DATE, -1);
					
					((Map<String, Object>) params.get("filter")).put("dateFrom", dateFormat.format(dateFrom.getTime()));
					((Map<String, Object>) params.get("filter")).put("dateTo", dateFormat.format(dateFrom.getTime()));
					
					String fileName = RunUserReport.createReport(params) + ".xlsx";
					String filePath = RunUserReport.path + "/reports/" + fileName;
					String subject = "Skyline Report " + params.get("reportName");
					String message = getEmailBody(params);
					for (String recipient: (ArrayList<String>)schedule.get("recipients")) {
						sendMail(recipient, subject, message, fileName, filePath);
						logReport(params, recipient);
					}
					
					File file = new File(filePath);
					file.delete();
					
					break;
				}
				
				case "weekly": {
					
					now = Calendar.getInstance();
					
					if (now.get(Calendar.DAY_OF_WEEK) == 2) {
						
						dateFrom.add(Calendar.WEEK_OF_YEAR, -1);
						dateFrom.set(Calendar.DAY_OF_WEEK, dateFrom.getFirstDayOfWeek());
						
						dateTo = Calendar.getInstance();
						dateTo.set(Calendar.DAY_OF_WEEK, 1);
						
						((Map<String, Object>) params.get("filter")).put("dateFrom", dateFormat.format(dateFrom.getTime()));
						((Map<String, Object>) params.get("filter")).put("dateTo", dateFormat.format(dateTo.getTime()));
						
						String fileName = RunUserReport.createReport(params) + ".xlsx";
						String filePath = RunUserReport.path + "/reports/" + fileName;
						String subject = "Skyline Report " + params.get("reportName");
						String message = getEmailBody(params);
						for (String recipient: (ArrayList<String>)schedule.get("recipients")) {
							sendMail(recipient, subject, message, fileName, filePath);
							logReport(params, recipient);
						}
						
						File file = new File(filePath);
						file.delete();
						
					}
					
					break;
				}
				
				case "monthly": {
					
					now = Calendar.getInstance();
					
					if (now.get(Calendar.DAY_OF_MONTH) == 1) {
						
						dateFrom = Calendar.getInstance();
						dateFrom.add(Calendar.MONTH, -1);
						dateFrom.set(Calendar.DATE, 1);

						dateTo = Calendar.getInstance();
						dateTo.set(Calendar.DAY_OF_MONTH, 1);
						
						((Map<String, Object>) params.get("filter")).put("dateFrom", dateFormat.format(dateFrom.getTime()));
						((Map<String, Object>) params.get("filter")).put("dateTo", dateFormat.format(dateTo.getTime()));
						
						String fileName = RunUserReport.createReport(params) + ".xlsx";
						String filePath = RunUserReport.path + "/reports/" + fileName;
						String subject = "Skyline Report " + params.get("reportName");
						String message = getEmailBody(params);
						for (String recipient: (ArrayList<String>)schedule.get("recipients")) {
							sendMail(recipient, subject, message, fileName, filePath);
							logReport(params, recipient);
						}
						
						File file = new File(filePath);
						file.delete();
						
					}
					
					break;
				}
				
			}
			
		}
		
	}
	
	
	
	public static ArrayList<Map<String, Object>> query(String q) throws Exception {
		
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(RunUserReport.dataSource);
    	
    	@SuppressWarnings("unchecked")
		List<Map<String, Object>> result = jdbcTemplate.queryForList(q);
    	
    	return (ArrayList<Map<String, Object>>) result;
    	
    }
	
	
	
	public static void queryInsert(String q, Map<String, Object> params) {
		
		NamedParameterJdbcTemplate template = new NamedParameterJdbcTemplate(RunUserReport.dataSource);
		
		template.execute(q, params, new PreparedStatementCallback() {
			@Override
			public Object doInPreparedStatement(java.sql.PreparedStatement ps) throws SQLException, DataAccessException {
				return ps.executeUpdate();
			}
		});
		
	}
	
	
	
	private static void sendMail(String to, String subject, String body, String fileName, String filePath) {
		
		String from = "noreply@skylinecms.co.uk";
		
	    Properties properties = System.getProperties();
	    
	    properties.setProperty("mail.smtp.host", RunUserReport.mailHost);
	    properties.setProperty("mail.user", RunUserReport.mailUserName);
	    properties.setProperty("mail.password", RunUserReport.mailPassword);
	    
	    Session session = Session.getDefaultInstance(properties);
	    
		try {
			
		    //Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);
			
			//Set From: header field of the header.
			message.setFrom(new InternetAddress(from));
			
			//Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			
			//Set Subject: header field
			message.setSubject(subject);
			
			//Create the message part 
			BodyPart messageBodyPart = new MimeBodyPart();
			
			//Fill the message
			messageBodyPart.setContent(body, "text/html");
			
			//Create a multipar message
			Multipart multipart = new MimeMultipart();
			
			//Set text message part
			multipart.addBodyPart(messageBodyPart);
			
			//Part two is attachment
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(filePath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);
			
			//Send the complete message parts
			message.setContent(multipart);
			
			//Send message
			Transport.send(message);
			
		} catch (MessagingException e) {
			
			e.printStackTrace();
		    
		}
		
	}
	
	
	
	private static String getEmailBody(Map<String, Object> params) throws Exception {
		
		String body = null;
		
		String q = "SELECT Message FROM email WHERE EmailCode = 'automated_report'";
		ArrayList<Map<String, Object>> result = query(q);
		
		if (result.get(0).get("Message") != null) {
			
			body = (String) result.get(0).get("Message");
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
			Calendar now = Calendar.getInstance();
			String date = dateFormat.format(now.getTime());
			body = body.replace("[date]", date);
			
			body = body.replace("[createdBy]", (String) params.get("userName"));
			body = body.replace("[reportName]", (String) params.get("reportName"));
			body = body.replace("[reportDescription]", (String) params.get("reportDescription"));
			
		} else {
			
			body = "";
			
		}
		
		return body;
		
	}
	
	
	
	private static void logReport(Map<String, Object> params, String recipient) {
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar now = Calendar.getInstance();
		String date = dateFormat.format(now.getTime());
		
		String q = 	"INSERT INTO	report_log" +
					"				(" +
					"					ReportID," +
					"					UserID," +
					"					UserFullName," +
					"					ReportName," +
					"					ReportDescription," +
					"					Recipient," +
					"					Date" +
					"				)" +
					"VALUES " +
					"				(" +
					"					:reportID," +
					"					:userID," +
					"					:userFullName," +
					"					:reportName," +
					"					:reportDescription," +
					"					:recipient," +
					"					:date" +
					"				)";
		
		Map<String, Object> args = new HashMap<String, Object>();
		args.put("reportID", params.get("reportID"));
		args.put("userID", params.get("userID"));
		args.put("userFullName", params.get("userName"));
		args.put("reportName", params.get("reportName"));
		args.put("reportDescription", params.get("reportDescription"));
		args.put("recipient", recipient);
		args.put("date", date);
		
		queryInsert(q, args);
		
	}
	
	
	
}


