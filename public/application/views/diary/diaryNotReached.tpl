
    <html>
        <head>
   <link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" /> 
        <link rel="stylesheet" href="{$_subdomain}/css/contextMenu/jquery.contextMenu.css" type="text/css" charset="utf-8" /> 
    {* Base CSS *}
	<link rel="stylesheet" href="{$_subdomain}/css/base_css.php?size=950" type="text/css" media="screen" charset="utf-8" />
        
	{* Skin CSS *}
	<link rel="stylesheet" href="{$_subdomain}/css/skin_css.php?skin={$_theme}&size=950" type="text/css" media="screen" charset="utf-8" />
	
        {* Site Stylesheet *}
       <script type="text/javascript" src="{$_subdomain}/js/jquery-1.7.1.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script> 
        <script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="{$_subdomain}/js/jquery.contextMenu.js"></script>
       
      
<!--        <script type="text/javascript" src="https://www.google.com/jsapi"></script>-->
<script>
    $(document).ready(function() {
    $.colorbox({ html:$('#h12s').html(), title:"Appointment cannot be reached"});
$('#cboxLoadedContent').css('width','750px');
$('#cboxLoadedContent').css('height','500px');
});
</script>
  
    </head>
    <body >
    
    <div id="h12s" style="display: none">
<div id="deleteAppointment" style="width:340px;height:350px;" align="center" >
    <fieldset style="width:300px;">
    <legend style="font-size:12px">Warning!</legend>
    <div align=center style="margin-top:20px;">
        
       
           
        
            <br>
               
            <p style="margin:none;padding:none;margin-bottom:30px;">
                The geographical location of this appointment is outside the engineers travel distance for today.  
                Please cancel this insertion and select an alternative day. 
                <br><br>Please note it may be possible to book other appointments within his geographical area.
                <br><br>
                Alternatively ask your supervisor to extend the working hours of the 
                "{$appDet.SkillsetName}" engineers which are working on 
                "{$appDet.AppointmentDate|date_format:"%d/%m/%Y"}" and attempt to book this appointment again.
                </p>
             <form method="post" action="{$_subdomain}/Diary/notReached">     
                  <input type="hidden" name="delete" value="{$del}">

            
                     <button type="button" class="btnStandard"  onclick="$('.ansd').val('0');this.form.submit()"  style="width:120px;float:right">Cancel insertion</button>
                     <input type="hidden" id="ansd" class="ansd" name="ans" value="">
               
        </form>
    
    </div>
  </fieldset>
           
    </div>
</div>
<!--        end job  details field set    -->
<!--     $('.ansd').val('1');this.form.submit()    -->
<!--<button  class="gplus-red" type="button" style="background-color:red;color:white;width:140px" onclick="alert('Unfortunately the engineers are at full capacity, if you wish to continue with this booking please contact your supervisor.')"  >Insert appointment</button>-->
</body>
     </html>
 
         