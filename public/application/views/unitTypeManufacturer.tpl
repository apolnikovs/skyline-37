{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $UnitTypeManufacturer}
{/block}

{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
             width:300px;
         }  
    </style>
{/block}
{block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}

    
    <script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
    <script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>


<script type="text/javascript">
      
    var checked = false;
  
    var $statuses = [
			{foreach from=$statuses item=st}
			   ["{$st.Name}", "{$st.Code}"],
			{/foreach}
                    ]; 
     
    var table;             
                    
        
    function gotoEditPage($sRow) {
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
    }


    /**
    *  This is used to change color of the row if its in-active.
    */
   
    function inactiveRow(nRow, aData) {
          
	if(aData[3] == $statuses[1][1] || aData[4] == $statuses[1][1]) {  
            $(nRow).addClass("inactive");
            //$('td:eq(2)', nRow).html($statuses[1][0]);
        } else if(aData[3] == $statuses[0][1] || aData[4] == $statuses[0][1]) {
            $(nRow).addClass("");
            //$('td:eq(2)', nRow).html($statuses[0][0]);
        }
        
    }
    
    
    var currentVal;
   
    /*
    function test() {
	alert("aliard");
    }
    */


    $(document).ready(function() {
        $("#mId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/unitTypeManufacturer/'+urlencode($("#mId").val()));
            }
        });
	//Click handler for finish button.
	$(document).on('click', '#finish_btn', function() {
	    $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');
	});

             

	//Add a change handler to the network dropdown - strats here
	/*$(document).on('change', '#mId', function() {
	    $(location).attr('href', '{$_subdomain}/ProductSetup/unitTypeManufacturer/'+urlencode($("#mId").val())); 
	});*/
	//Add a change handler to the network dropdown - ends here
                      
                     
	/* =======================================================
	*
	* set tab on return for input elements with form submit on auto-submit class...
	*
	* ======================================================= */

        $('input[type=text],input[type=password]').keypress(function(e) {
	    if(e.which == 13) {
		$(this).blur();
		if($(this).hasClass('auto-submit')) {
		    $('.auto-hint').each(function() {
			$this = $(this);
			if($this.val() == $this.attr('title')) {
			    $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
			    if($this.hasClass('auto-pwd')) {
				$this.prop('type','password');
			    }
			}
		    });
		    $(this).get(0).form.onsubmit();
		} else {
		    $next = $(this).attr('tabIndex') + 1;
		    $('[tabIndex="'+$next+'"]').focus();
		}
		return false;
	    }
	});        
        

			
	    $columnsList = [ 
		/* UnitTypeManufacturerID */    { bVisible: false },    
		/* ManufacturerName */  null,
		/* UnitTypeName */ null,
                /* Product Group */ null,
		/* Status */	    null
	    ];
                 
     
     
	{if $SupderAdmin eq true}
	    $displayButtons = "UA";
	    $dblclickCallbackMethod = "gotoEditPage";
	    $tooltipTitle = "{$page['Text']['tooltip_title']|escape:'html'}";
	{else}
	    $displayButtons = "";
	    $dblclickCallbackMethod = "";
	    $tooltipTitle = "";
	{/if}
         
	 
	table = $('#UnitTypesResults').PCCSDataTable({

	    aoColumns:			$columnsList,
	    aaSorting:			[[ 1, "asc" ]],   
	    displayButtons:		$displayButtons,
	    addButtonId:		'addButtonId',
	    addButtonText:		'{$page['Buttons']['insert']|escape:'html'}',
	    createFormTitle:		'{$page['Text']['insert_page_legend']|escape:'html'}',
	    createAppUrl:		'{$_subdomain}/ProductSetup/unitTypeManufacturer/insert/'+urlencode("{$mId}")+'/',
	    createDataUrl:		'{$_subdomain}/ProductSetup/ProcessData/UnitTypeManufacturer/',
	    createFormFocusElementId:   'UnitTypeName',
	    formInsertButton:		'insert_save_btn',

	    frmErrorRules: {
		UnitTypeName: { required: true }

	    },

	    frmErrorMessages: {
		UnitTypeName: { required: "{$page['Errors']['name']|escape:'html'}" }     
	    },                     

	    popUpFormWidth:		750,
	    popUpFormHeight:		430,
	    updateButtonId:		'updateButtonId',
	    updateButtonText:		'{$page['Buttons']['edit']|escape:'html'}',
	    updateFormTitle:		'{$page['Text']['update_page_legend']|escape:'html'}',
	    updateAppUrl:		'{$_subdomain}/ProductSetup/unitTypeManufacturer/update/'+urlencode("{$mId}")+'/',
	    updateDataUrl:		'{$_subdomain}/ProductSetup/ProcessData/UnitTypeManufacturer/',
	    formUpdateButton:		'update_save_btn',
	    updateFormFocusElementId:   'UnitTypeName',
	    colorboxFormId:		"UnitTypesForm",
	    frmErrorMsgClass:		"fieldError",
	    frmErrorElement:		"label",
	    htmlTablePageId:		'UnitTypesResultsPanel',
	    htmlTableId:		'UnitTypesResults',
	    fetchDataUrl:		'{$_subdomain}/ProductSetup/ProcessData/UnitTypeManufacturer/fetch/'+urlencode("{$mId}"),
	    formCancelButton:		'cancel_btn',
	    fnRowCallback:		'inactiveRow',
	    searchCloseImage:		'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
	    dblclickCallbackMethod:	$dblclickCallbackMethod,
	    tooltipTitle:		$tooltipTitle,
	    iDisplayLength:		25,
	    formDataErrorMsgId:		"suggestText",
	    frmErrorSugMsgClass:	"formCommonError",
	    sDom:			'ft<"#dataTables_command">rpli',
	    bottomButtonsDivId:		'dataTables_command'

	});
                      
		      
	$(document).on("click", "#checkAll", function() {

	    if(!checked) {
		$(".checkBoxDataTable").each(function() {
		    $(this).attr("checked","checked");
		});
		checked = true;
		$("#checkAll label").text("Uncheck All");
	    } else {
		$(".checkBoxDataTable").each(function() {
		    $(this).removeAttr("checked");
		});
		checked = false;
		$("#checkAll label").text("Check All");
	    }
	    
	    return false;

	});
                   

    });

    </script>

    
{/block}


{block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home">

               <div class="ServiceAdminTopPanel" >
                    <form id="UnitTypesTopForm" name="UnitTypesTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="ServiceAdminResultsPanel" id="UnitTypesResultsPanel" >
                    

                        <form name="listsForm" id="unitTypeListsForm" >
                        {$page['Labels']['manufacturer_label']|escape:'html'}
                        <select name="mId" id="mId" >
                            <option value="" {if $mId eq ''}selected="selected"{/if}>{$page['Text']['select_manufacturer']|escape:'html'}</option>

                            {foreach $manufacturers as $manufacturer}

                                <option value="{$manufacturer.ManufacturerID}" {if $mId eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                        </form>

                    <br><br>
                    <form id="UnitTypesResultsForm" class="dataTableCorrections">
                        <table id="UnitTypesResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Labels']['unit_type']|escape:'html'}" >{$page['Text']['unit_type']|escape:'html'}</th>
                                            <th title="{$page['Labels']['manufacturer']|escape:'html'}" >{$page['Text']['manufacturer_product_code']|escape:'html'}</th>
                                            <th title="{$page['Labels']['product_group']|escape:'html'}" >{$page['Text']['manufacturer_product_code']|escape:'html'}</th>
                                            <th width="10%" title="{$page['Labels']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>

                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                        <input type="hidden" name="sltUnitTypes" id="sltUnitTypes" >
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
               
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    


                </div>        
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               
                 
                 {if $SupderAdmin eq false}

                   <input type="hidden" name="addButtonId" id="addButtonId" > 
                   <input type="hidden" name="updateButtonId" id="updateButtonId" > 

                {/if} 

    </div>
                        
                        



{/block}



