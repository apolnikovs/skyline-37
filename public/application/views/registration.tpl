{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "New User Registration"}
{$PageId = $RegistrationPage}
{/block}

{block name=scripts}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*} 

<script type="text/javascript">

$(document).ready( function() {

    /* =======================================================
     *
     * set focus on first input field...
     *
     * ======================================================= */
     
    $('#branch').focus();
    
    /* =======================================================
     *
     * Initialise Form Validation
     *
     * ======================================================= */
    
    $('#registration_btn').click( function() {

           $('.auto-hint').each(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
           } );
    } );
    
    $('#registrationForm').validate( {
        rules: {
            title: {
                required: true
            },
            forename: {
                required: true
            },
            surname: {
                required: true
            },
            email: {
                email: true
            },
            username: {
                required: true,
                // Regex: Alphanumeric No Spaces
                pattern: /^[a-zA-Z0-9]+$/ 
            },             
            pwd: {
                required: true,
                minlength: 6,
                // Regex: at least 1 Capital Letter and 1 Numeric
                pattern: /^.*(?=.*\d)(?=.*[A-Z]).*$/
            },           
            pwd2: {
                required: true,
                equalTo: "#pwd"
            },
            question: {
                required: true
            },
            answer: {
                required: true
            }
        },
        messages: {
             title: {
                required: '{$page['Errors']['required']}'
            },
            forename: {
                required: '{$page['Errors']['required']}'
            },
            surname: {
                required: '{$page['Errors']['required']}'
            },
            email: {
                email: '{$page['Errors']['required']}'
            },
            username: {
                required: '{$page['Errors']['required']}',
                pattern: '{$page['Errors']['invalid']}'
            },             
            pwd: {
                required: '{$page['Errors']['required']}',
                minlength: '{$page['Errors']['bad_length']}',
                pattern: '{$page['Errors']['invalid']}'
            },           
            pwd2: {
                required: '{$page['Errors']['required']}',
                equalTo: '{$page['Errors']['no_match']}'
            },
            question: {
                required: '{$page['Errors']['required']}'
            },
            answer: {
                required: '{$page['Errors']['required']}'
            }
        },
        errorPlacement: function(error, element) {
                            error.insertAfter( element.parent() );
                        },
       errorClass: 'fieldError',
       ignoreTitle: true,
       invalidHandler: function(form, validator) {
                            $('.auto-hint-hide').each(function() {
                                    $this = $(this);
                                    if ($this.val() == '' && $this.attr('title') != '')  {
                                        $this.val($this.attr('title')).removeClass('auto-hint-hide').addClass('auto-hint');
                                        if ($this.hasClass('auto-pwd')) {
                                            $this.prop('type','text');
                                        }
                                    }
                            } );
                        }
    } );
    
    /* =======================================================
     *
     * Initialise input auto-hint functions...
     *
     * ======================================================= */
    
    $('.auto-hint').focus(function() {
            $this = $(this);
            if ($this.val() == $this.attr('title')) {
                $this.val('').removeClass('auto-hint');
                if ($this.hasClass('auto-pwd')) {
                    $this.prop('type','password');
                }
            }
        } ).blur(function() {
            $this = $(this);
            if ($this.val() == '' && $this.attr('title') != '')  {
                $this.val($this.attr('title')).addClass('auto-hint');
                if ($this.hasClass('auto-pwd')) {
                    $this.prop('type','text');
                }
            }         
        } ).each(function(){
            $this = $(this);
            if ($this.attr('title') == '') { return; }
            if ($this.val() == '') { 
                if ($this.attr('type') == 'password') {
                    $this.addClass('auto-pwd').prop('type','text');
                }
                $this.val($this.attr('title')); 
            } else { 
                $this.removeClass('auto-hint'); 
            }
            $this.attr('autocomplete','off');
        } );
    
    /* =======================================================
     *
     * set tab on return for input elements with form submit on auto-submit class...
     *
     * ======================================================= */
     
    $('input[type=text],input[type=password]').keypress( function( e ) {
            if (e.which == 13) {  
                $(this).blur();
                if ($(this).hasClass('auto-submit')) {
                    $('.auto-hint').each(function() {
                        $this = $(this);
                        if ($this.val() == $this.attr('title')) {
                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                            if ($this.hasClass('auto-pwd')) {
                                $this.prop('type','password');
                            }
                        }
                    } );
                    $(this).get(0).form.onsubmit();
                } else {
                    $next = $(this).attr('tabIndex') + 1;
                    $('[tabIndex="'+$next+'"]').focus();
                }
                return false;
            }   
        } ); 
       
});

</script>

{/block}

{block name=body}
<div class="main" id="registration">
        
       <form id="registrationForm" name="registrationForm" method="post" action="{$_subdomain}/Login/registration" class="prepend-4 span-16 last inline">
    
           <fieldset> 

               <legend title="IM5007">{$page['Text']['legend']|escape:'html'}</legend>
               {* <div class="serviceInstructionsBar">IM5007</div> *}

                
                <p class="information">
                    {$page['Text']['instructions']|escape:'html'}
                </p>
                
                <p class="information">
                    {$page['Text']['please_enter']|escape:'html'}
                    <br /><br />
                </p>

                <p>
                    <label>{$page['Labels']['department']|escape:'html'}:</label>
                    <span class="text" >location type here...</span>
                </p>
                
                <p>
                    <label>{$page['Labels']['brand']|escape:'html'}:</label>
                    <span class="text">brand here...</span>
                </p>
                
                <p>
                    <label for="branch">{$page['Labels']['branch']|escape:'html'}:</label>
                    <select name="branch" id="branch" class="text" tabIndex="1" >
                        <option value="" >{$page['Text']['select_branch']|escape:'html'}</option>
                        {foreach $branches as $b}
                        <option value="{$b.BranchID}">{$b.BranchName|escape:'html'}</option>
                        {/foreach}
                    </select>
                </p>
                
                <p>
                    <label for="title">{$page['Labels']['title']|escape:'html'}:<sup>*</sup></label>
                    <input type="text" name="title" id="title" value="" class="text" tabIndex="2" />
                </p>
                
                <p>
                    <label for="forename">{$page['Labels']['forename']|escape:'html'}:<sup>*</sup></label>
                    <input type="text" name="forename" id="forename" value="" class="text" tabIndex="3" />
                </p>
                
                <p>
                    <label for="surname">{$page['Labels']['surname']|escape:'html'}:<sup>*</sup></label>
                    <input type="text" name="surname" id="surname" value="" class="text" tabIndex="4" />
                </p>
                
                <p>
                    <label for="email">{$page['Labels']['email']|escape:'html'}:</label>
                    <input type="text" name="email" id="email" value="" title="{$page['Hints']['email']|escape:'html'}" class="text auto-hint" tabIndex="5" />
                </p>
                
                <p>
                    <label for="mobile">{$page['Labels']['mobile']|escape:'html'}:</label>
                    <input type="text" name="mobile" id="mobile" value="" title="{$page['Hints']['mobile']|escape:'html'}" class="text auto-hint" tabIndex="6" />
                </p>
                
                <p>
                    <label for="username">{$page['Labels']['username']|escape:'html'}:<sup>*</sup></label>
                    <input type="text" name="username" id="username" value="" title="{$page['Hints']['username']|escape:'html'}" class="text auto-hint" tabIndex="7" />
                </p>
                
                <p>
                    <label for="pwd">{$page['Labels']['pwd']|escape:'html'}:<sup>*</sup></label>
                    <input type="password" name="pwd" id="pwd" value="" title="{$page['Hints']['pwd']|escape:'html'}" class="text auto-hint" tabIndex="8" />
                </p>
                
                <p>
                    <label for="pwd2">{$page['Labels']['pwd2']|escape:'html'}:<sup>*</sup></label>
                    <input type="password" name="pwd2" id="pwd2" value="" title="{$page['Hints']['pwd2']|escape:'html'}" class="text auto-hint" tabIndex="9" />
                </p>
                
                <p>
                    <label for="question">{$page['Labels']['question']|escape:'html'}:<sup>*</sup></label>
                    <select name="question" id="question" class="text" tabIndex="10" >
                        <option value="" >{$page['Text']['select_question']|escape:'html'}</option>
                        {foreach $questions as $q}
                        <option value="{$q|escape:'html'}">{$q|escape:'html'}</option>
                        {/foreach}
                    </select>
                </p>
                
                <p>
                    <label for="answer">{$page['Labels']['answer']|escape:'html'}:<sup>*</sup></label>
                    <input type="text" name="answer" id="answer" value="" title="{$page['Hints']['answer']|escape:'html'}" class="text auto-hint auto-submit" tabIndex="11" />
                </p>
                
                {if $error ne ''}
                <p id="error" class="formError" style="text-align: center;" >{$error|escape:'html'}</p>
                {/if}
                
                  
                <p>
                    <div style="width:160px;font-size:1.2em;text-align:right;">
                        <br />
                        <sup style="color:#E42019;font-size:1.2em;font-weight:bold;">*</sup><i>{$page['Text']['compulsory']|escape:'html'}&nbsp;</i>
                    </div>
                </p>
                
                <p style="text-align:center; margin: 0;">
                    <span style="display: inline-block; width: 312px; text-align: center; ">
                        <input type="submit" name="registration_btn" id="registration_btn"  class="textSubmitButton" value="{$page['Buttons']['submit']|escape:'html'}" tabIndex="12" />
                    </span>
                </p>
                  
                <p class="information"">
                    {$page['Text']['issue']}
                </p>

                <p style="text-align: center; margin: 0;">
                    <span style="display: inline-block; width: 312px; text-align: center;">
                        <a href="{$_subdomain}/Login/registrationQuery" tabIndex="-1">{$page['Buttons']['query']|escape:'html'}</a>
                    </span>
                </p>
                
                <p style="text-align: right;">
                    <a href="{$_subdomain}/index/index" tabIndex="13">{$page['Buttons']['cancel']|escape:'html'}</a>
                </p>
                
            </fieldset>
  
        </form>
    
</div>
{/block}