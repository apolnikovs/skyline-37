{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
        <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
                <fieldset>
                    <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
                    <p>

                        <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

                    </p>

                    <p>

                        <span class= "bottomButtons" >


                            <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                        </span>

                    </p>


                </fieldset>   
        </form>            


    </div>    
    
{else}  
    
    
    <script type="text/javascript" >
        
      function selectItem(li) {
      
    	//if( li == null ) return alert("No match!");

  	// if coming from an AJAX call, let's use the CityId as the value
  	if( !!li.extra ) var sValue = li.extra[0];

  	// otherwise, let's just display the value in the text box
  	//else var sValue = li.selectValue;
        
        $("#ManufacturerID").val(sValue);
        
        $('#ModelNumber').trigger('blur');
        
  	//alert("The value you selected was: " + sValue);
     } 
     
     function formatItem(row) {
    	return row[0] + " ("+"{$page['Text']['manufacturer']|escape:'html'}"+": " + row[2] + ")";
     }
              
        
    $(document).ready(function() {
        $("#NetworkID").combobox({
            change: function() {
                $CompletionStatusDropDownList = $ManufacturerDropDownList =  $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';   
                var $NetworkID = $("#NetworkID").val();
                if($NetworkID && $NetworkID!='')
                {
                    //Getting clients of network
                    $.post("{$_subdomain}/ProductSetup/productNumbers/getClients/"+urlencode($NetworkID)+"/", '', function(data){
                        var $networkClients = eval("(" + data + ")");
                        if($networkClients)
                        {
                            for(var $i=0;$i<$networkClients.length;$i++)
                            {
                                $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                            }
                        }
                        $("#ClientID").html($clientDropDownList);
                    });
                    //Getting manufacturers of network
                    $.post("{$_subdomain}/ProductSetup/productNumbers/getManufacturers/"+urlencode($NetworkID)+"/", '', function(data){
                        var $networkManufacturers = eval("(" + data + ")");
                        if($networkManufacturers)
                        {
                            for(var $i=0;$i<$networkManufacturers.length;$i++)
                            {
                                $ManufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                            }
                        }
                        $("#ManufacturerID").html($ManufacturerDropDownList);
                    });
                }
            }
        });
        $("#ClientID").combobox({
            change: function() {
                $utDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';        
                var $ClientID = $("#ClientID").val();
                if($ClientID && $ClientID!='')
                {
                    $.post("{$_subdomain}/ProductSetup/productNumbers/getUnitTypes/"+urlencode($ClientID)+"/", '', function(data){
                        var $clientUnitTypes = eval("(" + data + ")");
                        if($clientUnitTypes)
                        {
                            for(var $i=0;$i<$clientUnitTypes.length;$i++)
                            {
                                $utDropDownList += '<option value="'+$clientUnitTypes[$i]['UnitTypeID']+'" >'+$clientUnitTypes[$i]['UnitTypeName']+'</option>';
                            }
                        }
                        $("#UnitTypeID").html($utDropDownList);
                    });
                }
            }
        });
        $("#ManufacturerID").combobox();
        $("#UnitTypeID").combobox();
        $("#PaymentRoute").combobox();
            $("#ModelNumber").blur();
            
            //Change handler for ModelNumber box on update/insert popup page.
            $(document).on('focus', '#ModelNumber', 
                    function() {

                            
                            $("#ModelNumber").autocomplete(
                            "{$_subdomain}/ProductSetup/productNumbers/getModels/"+urlencode($("#NetworkID").val())+"/"+urlencode($("#ManufacturerID").val())+"/",
                                {
                                                delay:10,
                                                minChars:2,
                                                matchSubset:1,
                                                matchContains:1,
                                               // cacheLength:1,
                                                onItemSelect:selectItem,
                                                formatItem:formatItem,
                                                autoFill:false
                                }
                            );


                    }); 
    
    
    
            
                  
           
           
                
            /* =======================================================
            *
            * Initialise input auto-hint functions...
            *
            * ======================================================= */

            $('.auto-hint').focus(function() {
                    $this = $(this);
                    if ($this.val() == $this.attr('title')) {
                        $this.val('').removeClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','password');
                        }
                    }
                } ).blur(function() {
                    $this = $(this);
                    if ($this.val() == '' && $this.attr('title') != '')  {
                        $this.val($this.attr('title')).addClass('auto-hint');
                        if ($this.hasClass('auto-pwd')) {
                            $this.prop('type','text');
                        }
                    }         
                } ).each(function(){
                    $this = $(this);
                    if ($this.attr('title') == '') { return; }
                    if ($this.val() == '') { 
                        if ($this.attr('type') == 'password') {
                            $this.addClass('auto-pwd').prop('type','text');
                        }
                        $this.val($this.attr('title')); 
                    } else { 
                        $this.removeClass('auto-hint'); 
                    }
                    $this.attr('autocomplete','off');
                } ); 
                
                
                
                
         });       
        
    </script>
    
    
    <div id="ProductNumbersFormPanel" class="SystemAdminFormPanel" >
    
                <form id="ProductNumbersForm" name="ProductNumbersForm" method="post" action="#" class="inline" >
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText" ></label></p>
                            
                         <p>
                               <label ></label>
                               <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>
                         </p>
                       
                         
                         {if $SupderAdmin eq true} 
                         
                          
                          <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="NetworkID" id="NetworkID" class="text" >
                                <option value="" {if $datarow.NetworkID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $datarow.NetworkID eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                          
                          
                          <p>
                            <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $clients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                          
                        {else if $NetworkUser eq true}
                            
                           <p>
                            <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:</label>
                             &nbsp;&nbsp;  
                            <label class="labelHeight"  >  
                             {$datarow.NetworkName|escape:'html'}
                             <input type="hidden" name="NetworkID" value="{$datarow.NetworkID|escape:'html'}" >
                            </label> 
                          </p>
                          
                          <p>
                            <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ClientID" id="ClientID" class="text" >
                                <option value="" {if $datarow.ClientID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $clients as $client}

                                    <option value="{$client.ClientID}" {if $datarow.ClientID eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                          </p>
                          
                        {else}    
                          
                                <p>
                                    <label class="fieldLabel" for="NetworkID" >{$page['Labels']['network']|escape:'html'}:</label>
                                    &nbsp;&nbsp;  
                                    <label class="labelHeight"  >  
                                    {$datarow.NetworkName|escape:'html'}
                                    <input type="hidden" name="NetworkID" value="{$datarow.NetworkID|escape:'html'}" >
                                    </label> 
                                </p>
                          
                                <p>
                                    <label class="fieldLabel" for="ClientID" >{$page['Labels']['client']|escape:'html'}:<sup>*</sup></label>

                                    &nbsp;&nbsp;  
                                     <label class="labelHeight"  >  
                                    {$datarow.ClientName|escape:'html'}
                                    <input type="hidden" name="ClientID" value="{$datarow.ClientID|escape:'html'}" >
                                    </label> 
                               </p>
                        {/if}    
                         
                        
                        
                         <p>
                            
                             <label class="fieldLabel" for="ProductNo" >{$page['Labels']['product_number']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ProductNo" value="{$datarow.ProductNo|escape:'html'}" id="ProductNo" maxlength="10" >
                        
                         </p>
                        
                        
                          <p>
                            <label class="fieldLabel" for="ManufacturerID" >{$page['Labels']['manufacturer']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="ManufacturerID" id="ManufacturerID" class="text" >
                                <option value="" {if $datarow.ManufacturerID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $manufacturers as $manufacturer}

                                    <option value="{$manufacturer.ManufacturerID}" {if $datarow.ManufacturerID eq $manufacturer.ManufacturerID}selected="selected"{/if}>{$manufacturer.ManufacturerName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                         
                         
                         
                         <p>
                            
                             <label class="fieldLabel" for="ModelNumber" >{$page['Labels']['model']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text ac_input"  name="ModelNumber" value="{$datarow.ModelNumber|escape:'html'}"  id="ModelNumber" >
                             
                        
                         </p>
                        
                         <p>
                            
                             <label class="fieldLabel" for="ModelDescription" >{$page['Labels']['model_description']|escape:'html'}:<sup>*</sup></label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ModelDescription" value="{$datarow.ModelDescription|escape:'html'}" id="ModelDescription" >
                        
                         </p>
                        
                          <p>
                            <label class="fieldLabel" for="UnitTypeID" >{$page['Labels']['unit_type']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="UnitTypeID" id="UnitTypeID" class="text" >
                                <option value="" {if $datarow.UnitTypeID eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $unitTypes as $unitType}

                                    <option value="{$unitType.UnitTypeID}" {if $datarow.UnitTypeID eq $unitType.UnitTypeID}selected="selected"{/if}>{$unitType.UnitTypeName|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                         </p>
                        
                        
                         
                        
                        
                        
                         <p>
                            
                             <label class="fieldLabel" for="ActualSellingPrice" >{$page['Labels']['actual_selling_price']|escape:'html'}:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="ActualSellingPrice" value="{$datarow.ActualSellingPrice|escape:'html'}" id="ActualSellingPrice" >
                        
                         </p>
                         
                         
                         
                          <p>
                            
                             <label class="fieldLabel" for="AuthorityLimit" >{$page['Labels']['authority_limit']|escape:'html'}:</label>
                                &nbsp;&nbsp; 
                             <input  type="text" class="text"  name="AuthorityLimit" value="{$datarow.AuthorityLimit|escape:'html'}" id="AuthorityLimit" >
                        
                         </p>
                       
                          
                          <p>
                            <label class="fieldLabel" for="PaymentRoute" >{$page['Labels']['payment_route']|escape:'html'}:<sup>*</sup></label>
                           
                             &nbsp;&nbsp;  
                             <select name="PaymentRoute" id="PaymentRoute" class="text" >
                                <option value="" {if $datarow.PaymentRoute eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                                {foreach $paymentRoutes as $paymentRoute}

                                    <option value="{$paymentRoute.Code}" {if $datarow.PaymentRoute eq $paymentRoute.Code}selected="selected"{/if}>{$paymentRoute.Name|escape:'html'}</option>

                                {/foreach}
                                
                            </select>
                          </p>
                         
                          
                             
                        <p>
                            <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>

                            &nbsp;&nbsp; 

                                {foreach $statuses as $status}

                                    <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                                {/foreach}    
                        </p>
                            
                         

                        <p>

                            <span class= "bottomButtons" >

                                <input type="hidden" name="ProductID"  value="{$datarow.ProductID|escape:'html'}" >


                                {if $datarow.ProductID neq '' && $datarow.ProductID neq '0'}

                                        <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {else}

                                    <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >

                                {/if}

                                    <br>
                                    <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                            </span>

                        </p>

                           
                </fieldset>    
                        
                </form>        
                                             
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
