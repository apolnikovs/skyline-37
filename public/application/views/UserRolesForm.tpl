<style type="text/css">
    #search_results_test_filter label {
        left: 275px;
        display: inline;
    }
</style>
{if $accessErrorFlag eq true} 
    
    <div id="accessDeniedMsg" class="SystemAdminFormPanel"  >   
    
    <form id="accessDeniedMsgForm" name="accessDeniedMsgForm" method="post"  action="#" class="inline">
        <fieldset>
            <legend title="" > {$page['Text']['error_page_legend']|escape:'html'} </legend>
            <p>

                <label class="formCommonError" >{$accessDeniedMsg|escape:'html'}</label><br><br>

            </p>

            <p>

                <span class= "bottomButtons" >
                    <input type="submit" name="cancel_btn" 
                           class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"
                           value="{$page['Buttons']['ok']|escape:'html'}" >

                </span>

            </p>


        </fieldset>   
    </form>            


    </div>    
    
{else}  

    <script>
        function selectRow(nRow, aData){
        
           
        
            //console.info(aData[3]);
            
            if(aData[3] == null){
                $('td:eq(2)', nRow).html( '<input name="PermissionID_'+aData[0]+'" type="checkbox" />' );
            }else{
                $('td:eq(2)', nRow).html( '<input name="PermissionID_'+aData[0]+'" value="selected" type="checkbox" checked="checked" />' );
            }
       
        }
        
     $(document).ready(function(){
        $("#UserType").combobox();
        $( "#InactivityTimeout" ).spinner({
                                    min: 1,
                                    max: 300                                 
                                  }).change( function() {
                                    //alert('change event '+$(this).spinner('value'));
                                        if ( $(this).spinner('value') > 300 ) {
                                            $( this ).spinner( 'value', 300 );
                                        } else if ( $(this).spinner('value') < 1 ) {
                                            $( this ).spinner( 'value', 1 );
                                        }
                                    });
                                    
                                    

       
                                    
        userRolesDTTable("{$PermissionTagged}");
        
        $("input[name='PermissionTagged']").change(function(){
                    
                    // userRolesDTTable($(this).val());
                    
                    if($(this).val()!='')
                    {
                        $filterText = '"'+$(this).val()+'"';
                    }    
                    else
                    {
                        $filterText = '';
                    }
                   
                    
                    $("#search_results_test_filter input").val($filterText);
                    $("#search_results_test_filter input").trigger("keyup");
                    
                });
        
        
         
         
          
        $(document).on('click', '.DTCheckBox', function() { 

            var $chk_name = $(this).attr("name");

            var $ticked = false;
            
            if($(this).is(':checked'))
            {
                    $("#Hidden"+$chk_name).val('Active');
                    $(this).val("Tagged");
                    $ticked = true;
            }
            else
            {
                $("#Hidden"+$chk_name).val('In-active');
                $(this).val("Un-tagged");
            }
            
             countCheckBoxes();
             
             
              var data = oTable['search_results_test'].fnGetNodes();
              
              
              for(var i = 0; i < data.length; i++) {
                var el = $($(data[i]).find("td")[2]).find("input");
                
                
                if(el) 
                {
                    if($chk_name==el.attr("name"))
                    {    
                        $RowIndex = el.attr("id").replace('PermissionID_RowIndex_', '');    
                            
                        if($ticked)
                        {
                            $html_check_box = '<input class="DTCheckBox" id="'+el.attr("id")+'" name="'+el.attr("name")+'" value="Tagged" type="checkbox" checked="checked" />';
                        }
                        else
                        {
                           $html_check_box = '<input class="DTCheckBox" id="'+el.attr("id")+'" name="'+el.attr("name")+'" type="checkbox" value="Un-tagged" />';
                        }    
                        
                        oTable['search_results_test'].fnUpdate( $html_check_box, parseInt($RowIndex), 3 , false ) ; 
                        
                        break;
                    }
                   
                }
             }
             
           
           return false;
           
            
        });
         
         function countCheckBoxes()
         {
             var $TaggedCount   = 0;
             var $UnTaggedCount = 0;
             
             $('.HiddenPermissions').each(function() {
                                   
                                   
                                           
                               if($(this).val()=='Active')
                                   {
                                       $TaggedCount++;
                                   }
                               else
                                   {
                                        $UnTaggedCount++;
                                   }
                                        
                                        
                                    } );
                                    
                                    
            $("#TaggedPermissions").html($TaggedCount);                        
            $("#UntaggedPermissions").html($UnTaggedCount);
            $("#AllPermissions").html($TaggedCount+$UnTaggedCount);
             
         }
         
         
         //$("#search_results_test_filter").hide();
         
         

       
    }); 
    
    
    function userRolesDTTable($TaggedFlag)
    {
        $('#search_results_test').PCCSDataTable( {
            sDom:             'ft<"#dataTables_child">rpli',
            //displayButtons:     'P',
            oLanguage:          { "sEmptyTable": "No Appointments" },
            bServerSide:        false,
            bStateSave: true,
            htmlTableId:        'search_results_test',
            htmlTablePageId:    'search_results_test_panel',
            fetchDataUrl:       '{$_subdomain}/Data/permission/{$datarow.RoleID}/1'+'/'+$TaggedFlag+'/',
            aoColumns: [ 
                    { 'bVisible' : false, "bSearchable": false },
                    { 'bSortable' : true, "bSearchable": true },
                    { 'bSortable' : true, "bSearchable": true },
                    { 'bSortable' : false }
            ],
           // fnRowCallback:          'selectRow',
            bottomButtonsDivId:'dataTables_child',
            searchCloseImage:   '{$_subdomain}/css/Skins/{$_theme}/images/close.png',        
            popUpFormWidth:  0,
            popUpFormHeight: 0,
            aaSorting:	    [[ 1, "asc" ]],
           // sScrollY: '200px',
           // bScrollCollapse: false
            //bPaginate: false
            bDestroy:true      

        }); 
    }
    
    </script>
    
    <div id="serviceTypesFormPanel" class="SystemAdminFormPanel" >
    
                <form id="STForm" name="STForm" method="post"  action="#" class="inline">
       
                <fieldset>
                    <legend title="" >{$form_legend|escape:'html'}</legend>
                        
                    <p><label id="suggestText"></label></p>
                    <p>
                        <label ></label>
                        <span class="topText" >{$page['Text']['top_info_text1']|escape:'html'} <span>*</span> {$page['Text']['top_info_text2']|escape:'html'}</span>

                    </p>
                       
        
                    <p>
                    <label class="fieldLabel" for="Name" >{$page['Labels']['role_name']|escape:'html'}:<sup>*</sup></label>

                    &nbsp;&nbsp;
                    <input name="Name" class="text" id="Name" value="{$datarow.Name}" />

                    </p>
                         
                         
                    <p>
                    <label class="fieldLabel" for="UserType" >{$page['Labels']['user_type']|escape:'html'}:<sup>*</sup></label>

                        &nbsp;&nbsp; 
                        <select name="UserType" id="UserType" >

                            <option value="" {if $datarow.UserType eq ''}selected="selected"{/if}>{$page['Text']['select_default_option']|escape:'html'}</option>

                            {foreach $UserTypes as $type}

                                <option value="{$type|escape:'html'}" {if $datarow.UserType eq $type} selected="selected" {/if}>{$type|escape:'html'}</option>

                            {/foreach}

                        </select>
                    </p>
                    <p>
                        <label class="fieldLabel" for="InactivityTimeout" >{$page['Labels']['inactivity_timeout']|escape:'html'}:<sup>*</sup></label>
                        &nbsp;&nbsp;
                        <input name="InactivityTimeout" id="InactivityTimeout" style="width: 40px;" value="{$datarow.InactivityTimeout}" />&nbsp;Minutes&nbsp;&nbsp;&nbsp;
                        <span style="color: red;">Maximum Timeout 300 Minutes</span>
                    </p>
                    <p>
                    <label class="fieldLabel" for="Status" >{$page['Labels']['status']|escape:'html'}:<sup>*</sup></label>

                        &nbsp;&nbsp; 

                        {foreach $statuses as $status}

                            <input  type="radio" name="Status"  value="{$status.Code}" {if $datarow.Status eq $status.Code} checked="checked" {/if}  /> <span class="text" >{$status.Name|escape:'html'}</span> 

                        {/foreach}    



                    </p> 
                    <p>
                        <label class="fieldLabel" for="Comment" >{$page['Labels']['comment']|escape:'html'}:</label>
                        &nbsp;&nbsp;  <textarea id="Commment" name="Comment" rows="2" class="text" style="height:120px; width: 300px;">{$datarow['Comment']|escape:'html'}</textarea>
                    </p>
                    {if $datarow.RoleID neq '' && $datarow.RoleID neq '0'}
                        <span style="float:right; margin-top: 50px;" >
                            <input name="PermissionTagged" type="radio" value="Tagged" {if $PermissionTagged eq 'Active'} checked="checked" {/if} > {$page['Text']['tagged']|escape:'html'} (<span id="TaggedPermissions" >{$Tagged}</span>)
                            <input name="PermissionTagged" type="radio" value="Un-tagged" {if $PermissionTagged eq 'In-active'} checked="checked" {/if} > {$page['Text']['untagged']|escape:'html'} (<span id="UntaggedPermissions" >{$Untagged}</span>)
                            <input name="PermissionTagged" type="radio" value="" {if $PermissionTagged eq 'Both'} checked="checked" {/if} > {$page['Text']['both']|escape:'html'} (<span id="AllPermissions" >{$Both}</span>)
                        </span>
                    {/if}
                    
                    <div class="LTResultsPanel" id="search_results_test_panel" >
                    <table id="search_results_test" border="0" cellpadding="0" cellspacing="0" class="browse">
                        <thead>
                            <tr>
                                <th></th>
                                <th style="width:49%"  >{$page['Labels']['permission_name']|escape:'html'}</th>
                                <th style="width:50%"  >{$page['Labels']['description']|escape:'html'}</th>
                                <th style="width:50px;"  >{$page['Labels']['select']|escape:'html'}</th>

                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table> 
                    </div>            
                    <p>

                        <span class= "bottomButtons" >

                            {foreach $TotalPermissions as $tp}
                            
                                 {if $tp.3}
                                     
                                     <input class="HiddenPermissions" id="HiddenPermissionID_{$tp.0}" name="HiddenPermissionID_{$tp.0}"  type="hidden" value="Active" />
                                     
                                 {else}
                                     
                                     <input class="HiddenPermissions" id="HiddenPermissionID_{$tp.0}" name="HiddenPermissionID_{$tp.0}" type="hidden" value="In-active" />
                                     
                                 {/if}    
                                     
                            {/foreach}
                            
                            <input type="hidden" name="RoleID"  value="{$datarow.RoleID}" >

                            {if $datarow.RoleID neq '' && $datarow.RoleID neq '0'}
                                <input type="submit" name="update_save_btn" class="textSubmitButton centerBtn" id="update_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                            {else}
                                <input type="submit" name="insert_save_btn" class="textSubmitButton centerBtn" id="insert_save_btn"  value="{$page['Buttons']['save']|escape:'html'}" >
                            {/if}

                                <br />
                                <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['cancel']|escape:'html'}" >



                        </span>

                    </p>

                </fieldset>    
                        
                </form>        
                        
       
</div>
                 
{/if}  
                                    
                                    
{* This block of code is for to display message after data updation *} 

                                    
<div id="dataUpdatedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataUpdatedMsgForm" name="dataUpdatedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_updated_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>    

    
 
  {* This block of code is for to display message after data insertion *}                      
                        
<div id="dataInsertedMsg" class="SystemAdminFormPanel" style="display: none;" >   
    
    <form id="dataInsertedMsgForm" name="dataInsertedMsgForm" method="post"  action="#" class="inline">
            <fieldset>
                <legend title="" > {$form_legend|escape:'html'} </legend>
                <p>
                 
                    <b>{$page['Text']['data_inserted_msg']|escape:'html'}</b><br><br>
                 
                </p>
                
                <p>

                    <span class= "bottomButtons" >


                        <input type="submit" name="cancel_btn" class="textSubmitButton rightBtn" id="cancel_btn" onclick="return false;"  value="{$page['Buttons']['ok']|escape:'html'}" >

                    </span>

                </p>
                          
                
            </fieldset>   
    </form>            
    
    
</div>                            
                        
