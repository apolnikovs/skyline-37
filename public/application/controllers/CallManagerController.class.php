<?php

require_once('CustomSmartyController.class.php');

/**
 * Description
 *
 * Call Manager
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.01
 * 
 * Changes
 * Date        Version Author                 Reason
 * 30/12/2012  1.00    Nageswara Rao Kanteti  Initial Version      

 ******************************************************************************/

class CallManagerController extends CustomSmartyController {
    
    public $config;  
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;
    
    public function __construct() { 
        
        parent::__construct(); 
        
       /* ==========================================
         * Read Application Config file.
         * ==========================================
        */
        
        $this->config = $this->readConfig('application.ini');
        
       /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */
               
        $this->session = $this->loadModel('Session'); 
        
        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */
               
        $this->messages = $this->loadModel('Messages'); 
        
       // $this->smarty->assign('_theme', 'skyline');
        
       if (isset($this->session->UserID)) {
           
             
           

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser( $this->session->UserID );
            
            
            if(!$this->user->ServiceProviderID && !$this->user->SuperAdmin)
            {
                $this->redirect('index',null, null);
            }
            
            //$this->log(var_export($this->user, true));

            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            
             $this->smarty->assign('_theme', $this->user->Skin);
            
            if($this->user->BranchName)
            {
                $this->smarty->assign('logged_in_branch_name', " - ".$this->user->BranchName." ".$this->config['General']['Branch']." ");
            }
            else
            {
                $this->smarty->assign('logged_in_branch_name', "");
            }
            
            
            if($this->session->LastLoggedIn)
            {
                $this->smarty->assign('last_logged_in', " - ".$this->config['General']['LastLoggedin']." ".date("jS F Y G:i", $this->session->LastLoggedIn));
            }
            else
            {
                $this->smarty->assign('last_logged_in', '');
            } 
            
            
            $topLogoBrandID = $this->user->DefaultBrandID;

        } else {
            
            $topLogoBrandID = isset($_COOKIE['brand'])?$_COOKIE['brand']:0;
            
            $this->smarty->assign('session_user_id','');
            $this->smarty->assign('name','');
            $this->smarty->assign('last_logged_in','');
            $this->smarty->assign('logged_in_branch_name', '');   
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect('index',null, null);
            
        } 
        
        if($topLogoBrandID)
        {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo']))?$topBrandLogo[0]['BrandLogo']:'');
        }
        else
        {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);
        
        
         if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
       
    }
       
    
    
    
    
    /**
     * Description
     * 
     * This method is used to process the data i.e fething, updating and inserting.
     * 
     * @param array $args It contains modelname as first element where second element as process type
     * @return void It prints json encoded result where datatable using this data.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
   
     **************************************************************************/
     public function ProcessDataAction($args)
     {
            $modelName = isset($args[0])?$args[0]:'';
            $type      = isset($args[1])?$args[1]:'';
         
            if($modelName && $modelName!='')
            {
                
                $this->page =  $this->messages->getPage(lcfirst($modelName), $this->lang);
                
                
                $model  = $this->loadModel($modelName);
                
                if($type=='fetch')
                {
                    $_POST['firstArg']  = isset($args[2])?$args[2]:'';
                    $_POST['secondArg'] = isset($args[3])?$args[3]:'';
                    
                    $result     =   $model->fetch($_POST);
                }
                else
                {
                   // $this->log(var_export($_POST, true));
                    
                    
                    $result     = $model->processData($_POST);
                }    
                echo json_encode( $result );
           }
            return;
     }
    
    
    
    
    
     
    /**
     * Description
     * 
     * Call Manager
     * 
     * 
     * @param array $args
     * @return void  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
   
      public function indexAction( $args ) { 
            
           //$method = isset($args[0])?$args[0]:false;
           $number   = isset($args['n'])?$args['n']:'';
            
        
           $this->page    = $this->messages->getPage('callManager', $this->lang);
           $this->smarty->assign('page', $this->page);
         

           $JobModel = $this->loadModel('Job');
   
           $callerDetails = $JobModel->fetchCallerDetails(array(0=>$number));
           
            
           $this->smarty->assign('callerDetails', $callerDetails['aaData']);
           $this->smarty->assign('number', $number);
          
      
           
           $this->smarty->display('callManager.tpl'); 


           return;
        
       
      }
      
      
      /**
     * Description
     * 
     * It gets the customer details for given phone number and customer id.
     * 
     * @param array $args
     * @return void  
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
   
      public function fetchCallerDetailsAction( $args ) { 
            
           
           $number   = isset($args['n'])?$args['n']:'';
           $cuID     = isset($args['cuID'])?$args['cuID']:'';
            
           $JobModel = $this->loadModel('Job');
   
           $callerDetails = $JobModel->fetchCallerDetails(array(0=>$number, 1=>$cuID));
           
           $result = isset($callerDetails['aaData'][0])?$callerDetails['aaData'][0]:array();
           
           //$this->log($result);
           echo  json_encode($result);
        
      }
      
      
}


?>
