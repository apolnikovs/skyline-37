<?php

require_once('CustomSmartyController.class.php');

/**
 * Description
 *
 * This class is used for generating menu items under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.11
 * 
 * Changes
 * Date        Version Author                 Reason
 * ??/??/2012  1.00    Nageswara Rao Kanteti  Initial Version      
 * 26/06/2012  1.01    Andrew J. Williams     Added item link for general defaults
 * 27/09/2012  1.01A   Andrew J. Williams     Added Graphical Anlysis defaults 
 * 23/10/2012  1.02    Nageswara Rao Kanteti  Menu items alphabetically ordered 
 * 01/02/2013  1.03    Andrew J. Williams     Added Manufaturer's Product Groups Option
 * 21/02/2013  1.04    Vykintas Rutkunas      Secondary Service Provider Logo
 * 22/03/2013  1.05    Nageswara Rao Kanteti  Samsung Experience
 * 27/03/2013  1.06    Andrew J. Williams     Added System Status Permissions
 * 24/04/2013  1.07    Andris Polnikovs       Service Providers System admin access
 * 26/04/2013  1.08    Andris Polnikovs       Addet lookup table "Parts Order Status"
 * 04/06/2013  1.09    Andris Polnikovs       Addet lookup table "Job Fault Codes"
 * 11/06/2013  1.10    Andris Polnikovs       Addet lookup table "Part Fault Codes"
 * 12/06/2013  1.11    Andris Polnikovs       Addet lookup table "Shelf Location"
 * 01/07/2013          Thirumalesh Bandi      Added page title and page description dynamically for every page.
 * **************************************************************************** */
class SystemAdminController extends CustomSmartyController {

    public $config;
    public $session;
    public $skyline;
    public $messages;
    public $lang = 'en';
    public $page;
    public $statuses = array(
        0 => array('Name' => 'Active', 'Code' => 'Active'),
        1 => array('Name' => 'In-active', 'Code' => 'In-active')
    );

    public function __construct() {

        parent::__construct();

        /* ==========================================
         * Read Application Config file.
         * ==========================================
         */

        $this->config = $this->readConfig('application.ini');

        /* ==========================================
         *  Initialise Session Model
         * ==========================================
         */

        $this->session = $this->loadModel('Session');

        /* ==========================================
         *  Initialise Messages Model
         * ==========================================
         */

        $this->messages = $this->loadModel('Messages');

        //$this->smarty->assign('_theme', 'skyline');

        if (isset($this->session->UserID)) {

            $user_model = $this->loadModel('Users');
            $this->user = $user_model->GetUser($this->session->UserID);
            $this->smarty->assign('loggedin_user', $this->user);

            //echo("<pre>"); print_r($this->user); echo("</pre>");
            //$this->log(var_export($this->user, true));

            $this->smarty->assign('name', $this->user->Name);
            $this->smarty->assign('session_user_id', $this->session->UserID);
            $this->smarty->assign('_theme', $this->user->Skin);

            if ($this->user->BranchName) {
                $this->smarty->assign('logged_in_branch_name', " - " . $this->user->BranchName . " " . $this->config['General']['Branch'] . " ");
            } else {
                $this->smarty->assign('logged_in_branch_name', "");
            }


            if ($this->session->LastLoggedIn) {
                $this->smarty->assign('last_logged_in', " - " . $this->config['General']['LastLoggedin'] . " " . date("jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign('last_logged_in', '');
            }


            $topLogoBrandID = $this->user->DefaultBrandID;
        } else {

            $topLogoBrandID = isset($_COOKIE['brand']) ? $_COOKIE['brand'] : 0;

            $this->smarty->assign('session_user_id', '');
            $this->smarty->assign('name', '');
            $this->smarty->assign('last_logged_in', '');
            $this->smarty->assign('logged_in_branch_name', '');
            $this->smarty->assign('_theme', 'skyline');
            $this->redirect('index', null, null);
        }

        if ($topLogoBrandID) {
            $skyline = $this->loadModel('Skyline');
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);

            $this->smarty->assign('_brandLogo', (isset($topBrandLogo[0]['BrandLogo'])) ? $topBrandLogo[0]['BrandLogo'] : '');
        } else {
            $this->smarty->assign('_brandLogo', '');
        }
        $this->smarty->assign('showTopLogoBlankBox', false);


        if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
    }

    /**
     * Description
     * 
     * It generates system admin menu items (top and second level) based on parameter passing to it
     * 
     * @param array $args
     * @return void  It returns nothing but prints menu items on the page.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */
    public function indexAction($args) {

        //permission check to displat menu

       

        $this->page = $this->messages->getPage('SystemAdmin', $this->lang);
        
        
        $model = $this->loadModel('GroupHeadings'); 
        $menuItemList= $model->fetchMenuList(0);
       
       // var_dump($menuItemList);
      
      
         // Getting the values in multidimensional array.Based on ParentID.
         // 'nextLevel' is new array variable.
        
            for($i = 1; $i < count($menuItemList); $i++){
             
            $menuItemList[$i]['nextLevel']=$model->fetchMenuList($menuItemList[$i]['GID']);
           
         }
         
//echo "<pre>";
   // print_r($menuItemList);
    //exit();
    // $menuItemList['nextlevel'] = $nextlevel;
    

        
      /*   $this->log($menuItemList['nextlevel']);*/
     
     /*   $menuItemList = array(
            0 => array(
                'title' => 'System Defaults',
                'code' => 'systemDefaults',
                'link' => '/SystemAdmin/index/systemDefaults',
                'tooltip' => 'Click here to see System Defaults page',
                'shortDesc' => 'This menu item contains single choice questions that generally require a YES/NO answer or single option selection',
                'description' => 'This menu item contains single choice questions that generally require a YES/NO answer or single option selection'
            ),
            1 => array(
                'title' => 'Lookup Tables',
                'code' => 'lookupTables',
                'link' => '/SystemAdmin/index/lookupTables',
                'tooltip' => 'Click here to see Lookup Tables page',
                'shortDesc' => 'This menu item contains various lookup tables that are used throughout the system to select a single answer from a dropdown',
                'description' => 'This menu item contains various lookup tables that are used throughout the system to select a single answer from a dropdown',
                'nextLevel' => array(
                    0 => array('title' => 'Audit Trail Actions', 'link' => '/LookupTables/auditTrailActions', 'tooltip' => 'Click here to see Audit Trail Actions page', 'shortDesc' => 'This menu item contains a list of actions that will be assigned by the system when an audit trail record is created.   You cannot insert or delete a record, however, you can change its description or make it inactive.'),
                    1 => array('title' => 'Contact History Actions', 'link' => '/LookupTables/contactHistoryActions', 'tooltip' => 'Click here to see Contact History Actions page', 'shortDesc' => 'This menu item contains a list of Actions that will be assigned to a Contact History Record. You may insert new Actions as required.'),
                    2 => array('title' => 'Customer Titles', 'link' => '/LookupTables/customerTitles', 'tooltip' => 'Click here to see Customer Titles page', 'shortDesc' => 'This menu item contains a list of Customer titles.  You may add records, however, you should refrain from making this list extensively long.'),
                    3 => array('title' => "Counties", 'link' => '/LookupTables/county', 'tooltip' => "Click here to see Countries page", 'shortDesc' => "This menu item contains a list of Counties that can be selected from a drop down when inserting an address."),
                    4 => array('title' => "Countries", 'link' => '/LookupTables/country', 'tooltip' => "Click here to see Countries page", 'shortDesc' => 'This menu item contains a list of Countries that will be used when completing an address and assigning a Country to various system defaults.'),
                    5 => array('title' => "VAT Rates", 'link' => '/LookupTables/VATRates', 'tooltip' => 'Click here to see Edit VAT Rates page', 'shortDesc' => 'This menu item contains a list of VAT Codes and their associated rates that will be used to calculate VAT on a sales or service item.'),
                    6 => array('title' => "Job Types", 'link' => '/LookupTables/jobTypes', 'tooltip' => 'Click here to see Job Types page', 'shortDesc' => 'This menu item contains a list of Types of Job that will define the service required i.e. Installation, Repair, etc.'),
                    7 => array('title' => "Payment Types", 'link' => '/LookupTables/paymentTypes', 'tooltip' => 'Click here to see Payment Types page', 'shortDesc' => 'This menu item contains a list of payment types that will be allocated to any money received for a service and will be used in reports to reconcile payments received.'),
                    8 => array('title' => "Security Questions", 'link' => '/LookupTables/securityQuestions', 'tooltip' => 'Click here to see Security Questions page', 'shortDesc' => 'This menu item contains a list of questions that will prompt the user to remember their password.'),
                    9 => array('title' => "Service Types", 'link' => '/LookupTables/serviceTypes', 'tooltip' => 'Click here to see Service Types page', 'shortDesc' => 'This menu item contains a list of items that are related to Job Types and defines what fields are required and displayed on a job booking form.'),
                    10 => array('title' => "System Statuses", 'link' => '/LookupTables/systemStatuses', 'tooltip' => 'Click here to see System Statuses page', 'shortDesc' => 'This menu item contains a list Statuses that are used by the system to identify where a particular job is currently located during the repair life cycle.'),
                    11 => array('title' => "System Status Permissions", 'link' => '/LookupTables/systemStatusPermissions', 'tooltip' => 'Click here to see System Status Permissions page', 'shortDesc' => 'This menu item contains a list of brands, branches, clients, networks, roles or users and the permissions they can assign to a job, if they already have access to this facility.'),
                    12 => array('title' => "User Roles", 'link' => '/LookupTables/Roles', 'tooltip' => 'Click here to see User Types page', 'shortDesc' => "This menu item contains a list of role’s that are used to describe the job a particular user normally carries out within the business i.e. Branch Manager, Retail Assistant, etc."),
                    13 => array('title' => "RA Status Types", 'link' => '/LookupTables/RAStatusTypes', 'tooltip' => 'Click here to see RA Status Types page', 'shortDesc' => "This menu item will display a list of Statuses used by the Request Authorisation process."),
                    14 => array('title' => "Couriers", 'link' => '/LookupTables/couriers', 'tooltip' => 'Click here to see Couriers page', 'shortDesc' => "This menu item contains defaults that will be used to assign couriers and communicate with their automated processes."),
                    15 => array('title' => "Access Permissions", 'link' => '/LookupTables/accessPermissions', 'tooltip' => 'Click here to see Access Permissions page', 'shortDesc' => "This menu item contains a list of Access Permissions. It will allow the user to edit the Access Permission name and the Description.  These Access Permission are referenced in the User Roles table."),
                    16 => array('title' => "Accessories", 'link' => '/LookupTables/accessory', 'tooltip' => 'Click here to see Accessories page', 'shortDesc' => "This menu item contains a list of Accessories that can be assigned to specific unit types and displayed during the booking process."),
                    17 => array('title' => "Colours", 'link' => '/LookupTables/colour', 'tooltip' => 'Click here to see Colour page', 'shortDesc' => "This menu item contains a list of Colours that can be assigned to specific unit types and displayed during the booking process."),
                    18 => array('title' => "Product Codes", 'link' => '/LookupTables/ProductCode', 'tooltip' => 'Click here to see Product Codes page', 'shortDesc' => "This menu item contains a list of Product Codes that can be assigned to specific unit types and displayed during the booking process."),
                    19 => array('title' => "Currency", 'link' => '/LookupTables/Currency', 'tooltip' => 'Click here to see Currency page', 'shortDesc' => "This menu item contains a list of currency that can be selected when purchasing from international suppliers. User can maintain exchange rate in this table"),
                    20 => array('title' => "Mobile Phone Network", 'link' => '/LookupTables/mobilePhoneNetwork', 'tooltip' => 'Click here to see Mobile Phone Network page', 'shortDesc' => "This menu option contains a list of Mobile Phone Networks and their country of operation."),
                    21 => array('title' => "Part Order Status", 'link' => '/LookupTables/partOrderStatus', 'tooltip' => 'Click here to see Part Order Status page', 'shortDesc' => "This status is used on a part to identify the reason why a part order was raised."),
                    22 => array('title' => "SMS Messages", 'link' => '/LookupTables/sms', 'tooltip' => 'Click here to see SMS page', 'shortDesc' => "This menu item contains a list of SMS Messages.  It will allow the user to Insert new messages and Edit or Delete existing messages along with the Text that makes up the message."),
                    23 => array('title' => "Job Fault Codes ", 'link' => '/LookupTables/jobFaultCodes', 'tooltip' => 'Click here to see Job Fault Codes  page', 'shortDesc' => "Job Fault Codes are the field names the manufacturer requires when a warranty claim is submitted."),
                    24 => array('title' => "Job Fault Codes Lookups", 'link' => '/LookupTables/jobFaultCodesLookups', 'tooltip' => 'Click here to see Job Fault Codes Lookups  page', 'shortDesc' => "Job Fault Code Lookups are the selectable options that appear when entering job fault codes on a warranty claim."),
                    25 => array('title' => "Part Fault Codes", 'link' => '/LookupTables/partFaultCodes', 'tooltip' => 'Click here to see Part Fault Codes page', 'shortDesc' => "Part Fault Codes are the field names the manufacturer requires when a warranty claim is submitted."),
                    26 => array('title' => "Part Fault Codes Lookups", 'link' => '/LookupTables/partFaultCodesLookups', 'tooltip' => 'Click here to see Part Fault Codes Lookups  page', 'shortDesc' => "Part Fault Code Lookups are the selectable options that appear when entering part fault codes on a warranty claim"),
                    27 => array('title' => "Shelf Location", 'link' => '/LookupTables/shelfLocations', 'tooltip' => 'Click here to see Shelf Location  page', 'shortDesc' => "Shelf locations are the internal storage locations for stock items."),
                    28 => array('title' => "Part Location", 'link' => '/LookupTables/partLocations', 'tooltip' => 'Click here to see Part Location  page', 'shortDesc' => "Locations allow users to define where parts are stored internally and allowing multiple site users to configure each site location."),
                    29 => array('title' => "Repair Types ", 'link' => '/LookupTables/repairTypes', 'tooltip' => 'Click here to see Repair Types  page', 'shortDesc' => "Repair Types are used by Service Providers to configure how repairs are managed. For instance, some repair types may require warranty codes or be excluded from invoicing."),
                    30 => array('title' => "Part Categories", 'link' => '/LookupTables/partCategories', 'tooltip' => 'Click here to see Part Categories  page', 'shortDesc' => "Part Categories are a means for users to group parts in stock control."),
                    31 => array('title' => "Part Sub-Categories", 'link' => '/LookupTables/partSubCategories', 'tooltip' => 'Click here to see Part Sub-Categories  page', 'shortDesc' => "Sub-Categories are a means for users to further split categories in Stock Control."),
                    32 => array('title' => "Part Status Colours", 'link' => '/LookupTables/partStatusColours', 'tooltip' => 'Click here to see Part Status Colours  page', 'shortDesc' => "This menu option will allow a user to define a colour coding against each Part Status."),
                    33 => array('title' => "Group Headings", 'link' => '/LookupTables/groupHeadings', 'tooltip' => 'Click here to see Group Headings & Descriptive Text page', 'shortDesc' => "This menu item will allow editing of Group Headings and associated descriptive text for all user interface tables within the system."),
            )
                    ),
            2 => array(
                'title' => 'General Setup',
                'code' => 'generalSetup',
                'link' => '/SystemAdmin/index/generalSetup',
                'tooltip' => 'Click here to see General Setup page',
                'shortDesc' => 'This menu item contains a selection of generalised tables that define the system and the way it works',
                'description' => 'This menu item contains a selection of generalised tables that define the system and the way it works',
                'nextLevel' => array(
                    0 => array(
                        'title' => 'Access Permissions ',
                        'link' => '/generalSetup/AccessPermissions',
                        'tooltip' => 'Click here to see Audit Trail Actions page',
                        'shortDesc' => 'This page will allow the full list of tasks within Skyline which can be controlled by access levels to be entered/viewed.  The list should be a standard jquery browse with the following fields.'
                    ),
                    1 => array('title' => 'General Defaults', 'link' => '/GeneralSetup/GeneralDefault/', 'tooltip' => 'Click here to see General Default page', 'shortDesc' => "This menu item allows the edititing of the general defaults table.")
                )
            ),
            3 => array(
                'title' => 'Organisation Setup',
                'code' => 'organisationSetup',
                'link' => '/SystemAdmin/index/organisationSetup',
                'tooltip' => 'Click here to see Organisation Setup page',
                'shortDesc' => 'This menu item contains a selection of tables that define the structure of the organisations that use the system and their relationships',
                'description' => 'This menu item contains a selection of tables that define the structure of the organisations that use the system and their relationships',
                'nextLevel' => array(
                    0 => array('title' => 'System Users', 'link' => '/OrganisationSetup/systemUsers', 'tooltip' => 'Click here to see System Users page', 'shortDesc' => 'Page description goes here..'),
                    1 => array('title' => 'Service Networks', 'link' => '/OrganisationSetup/serviceNetworks', 'tooltip' => 'Click here to see Service Networks page', 'shortDesc' => 'This menu item contains a list of the service networks who are using Skyline.  You can insert or change a record from here.'),
                    2 => array('title' => 'Clients', 'link' => '/OrganisationSetup/clients', 'tooltip' => 'Click here to see Clients page', 'shortDesc' => 'This menu item contains a list of the clients for each Service Network.   You can insert, change or delete a record from here.'),
                    3 => array('title' => 'Brands', 'link' => '/OrganisationSetup/brands', 'tooltip' => 'Click here to see Brands page', 'shortDesc' => 'This menu item contains a list of the brands for each Client.  You can insert, change or delete a record from here.'),
                    4 => array('title' => 'Branches', 'link' => '/OrganisationSetup/branches', 'tooltip' => 'Click here to see Branches page', 'shortDesc' => 'This menu item contains a list of the branches for each Client.   You can insert, change or delete a record from here.'),
                    5 => array('title' => 'Service Providers', 'link' => '/OrganisationSetup/serviceProviders', 'tooltip' => 'Click here to see Service Providers page', 'shortDesc' => 'This menu item contains a list of Service Providers who are setup on the system.   You can insert, change or delete a record from here.'),
                    6 => array('title' => 'Extended Warrantor', 'link' => '/OrganisationSetup/extendedWarrantor', 'tooltip' => 'Click here to see Extended Warrantor page', 'shortDesc' => 'Description goes here..'),
                    7 => array('title' => 'Suppliers', 'link' => '/OrganisationSetup/suppliers', 'tooltip' => 'Click here to see Suppliers page', 'shortDesc' => 'Description goes here..')
                )
            ),
            4 => array(
                'title' => 'Product Setup',
                'code' => 'productSetup',
                'link' => '/SystemAdmin/index/productSetup',
                'tooltip' => 'Click here to see Product Setup page',
                'shortDesc' => 'This menu item contains a selection of tables that are used to create and maintain manufacturers and their products',
                'description' => 'This menu item contains a selection of tables that are used to create and maintain manufacturers and their products',
                'nextLevel' => array(
                    0 => array('title' => 'Manufacturers', 'link' => '/ProductSetup/manufacturers', 'tooltip' => 'Click here to see Manufacturers page', 'shortDesc' => 'Administer manufacturer and assign them to networks.'),
                    1 => array('title' => 'Unit Types', 'link' => '/ProductSetup/unitTypes', 'tooltip' => 'Click here to see Unit Types page', 'shortDesc' => 'Add and administer unit types and assign them to clients.'),
                    2 => array('title' => 'Models', 'link' => '/ProductSetup/models', 'tooltip' => 'Click here to see Models page', 'shortDesc' => 'Edit and administer the models database.'),
                    3 => array('title' => 'Pricing Structure', 'link' => '/ProductSetup/pricingStructure', 'tooltip' => 'Click here to see Pricing Structure page', 'shortDesc' => 'Assign a pricing structure for each unit type per client.'),
                    4 => array('title' => 'Product Numbers', 'link' => '/ProductSetup/productNumbers', 'tooltip' => 'Click here to see Product Numbers page', 'shortDesc' => 'Edit client product lists or catalogue.'),
                    5 => array('title' => 'Client Service Types', 'link' => '/ProductSetup/clientServiceTypes', 'tooltip' => 'Click here to see Client Service Types page', 'shortDesc' => 'Assign Unit Types to Clients.'),
                    6 => array('title' => "Manufacturer's Product Groups", 'link' => '/ProductSetup/unitTypeManufacturer', 'tooltip' => "Click here to see Manufacturer's Product Groups page", 'shortDesc' => 'Assign Product Groups for a Manufacturer to Unit Types.')
                )
            ),
            5 => array(
                'title' => 'Financial Setup', 'code' => 'financialSetup', 'link' => '/SystemAdmin/index/financialSetup', 'tooltip' => 'Click here to see Financial Setup page',
                'shortDesc' => 'This menu item contains tables and defaults that define the financial structure of the system and various billing processes',
                'description' => 'This menu item contains tables and defaults that define the financial structure of the system and various billing processes'
            ),
            6 => array(
                'title' => 'Text & Language Setup', 'code' => 'textLanguageSetup', 'link' => '/SystemAdmin/index/textLanguageSetup', 'tooltip' => 'Click here to see Text & Language Setup page',
                'shortDesc' => 'This menu item will allow the user to edit and maintain text that is displayed throughout the system and assign alternative names to selected fields',
                'description' => 'This menu item will allow the user to edit and maintain text that is displayed throughout the system and assign alternative names to selected fields'
            ),
            7 => array(
                'title' => 'Alternative Field Names', 'code' => 'alternativeFieldNames', 'link' => '/SystemAdmin/alternativeFieldNames', 'tooltip' => 'Click here to see Alternative Field Names page',
                'shortDesc' => 'This menu item will allow the user to change the field name displayed in Skyline with an alternative that is specific to their organisation',
                'description' => 'This menu item will allow the user to change the field name displayed in Skyline with an alternative that is specific to their organisation'
            ),
            8 => array(
                'title' => 'Procedures & Scheduled Tasks',
                'code' => 'procedures',
                'link' => '/SystemAdmin/index/procedures',
                'tooltip' => 'Click here to see Procedures & Scheduled Tasks page',
                'shortDesc' => 'This menu item contains various automated routines that interact with the system to update selected tables or allocate work and responsibilities',
                'description' => 'This menu item contains various automated routines that interact with the system to update selected tables or allocate work and responsibilities',
                'nextLevel' => array(
                )
            ),
            9 => array(
                'title' => 'Report Generator', 'code' => 'reportGenerator', 'link' => '/Report/index', 'tooltip' => 'Click here to see Report Generator page',
                'shortDesc' => 'This menu item will allow the user to access predefined reports or define and run new reports',
                'description' => 'This menu item will allow the user to access predefined reports or define and run new reports'
            ),
            10 => array(
                'title' => 'Job Allocation', 'code' => 'jobAllocation', 'link' => '/SystemAdmin/index/jobAllocation', 'tooltip' => 'Click here to see Job Allocation page',
                'shortDesc' => 'Job Allocation description goes here..',
                'description' => 'Job Allocation description goes here..',
                'accessPermission' => 'AP7005',
                'nextLevel' => array(
                    0 => array('title' => 'Central Service Allocations', 'link' => '/JobAllocation/centralServiceAllocations', 'tooltip' => 'Click here to see Central Service Allocations page', 'shortDesc' => 'This menu item allows products which are repaired centrally to be setup for allocation to one service centre.'),
                    1 => array('title' => 'Postcode Allocations', 'link' => '/JobAllocation/postcodeAllocations', 'tooltip' => 'Click here to see Postcode Allocations page', 'shortDesc' => 'This menu item allows service centres to be assigned postcode areas to allow jobs to be automatically assigned.'),
                    2 => array('title' => 'Town Allocations', 'link' => '/JobAllocation/townAllocations', 'tooltip' => 'Click here to see Town Allocations page', 'shortDesc' => 'This menu item allows service centres to be assigned a county or town.'),
                    3 => array('title' => 'Bought Out Guarantee', 'link' => '/JobAllocation/boughtOutGuarantee', 'tooltip' => 'Click here to see Bought Out Guarantee page', 'shortDesc' => 'This menu item allows manufacturer’s bought out guarantee products to be setup.'),
                    4 => array('title' => 'Unallocated Jobs', 'link' => '/JobAllocation/unallocatedJobs', 'tooltip' => 'Click here to see Unallocated Jobs page', 'shortDesc' => 'This menu item will allow jobs to be assigned to service centres or be cancelled if wrongly assigned.')
                )
            )
        );*/


        //$this->log("USER: " . var_export($this->user,true));
//	if($this->user->Username!="sa") {				//NETWORK
//	    
//	    array_splice($menuItemList,1,2);				//hide lookup tables
//	    array_splice($menuItemList,3,2);				//hide general setup
//	    array_splice($menuItemList[1]["nextLevel"],1,1);		//hide service networks
//	    
//	    if($this->user->UserType!="Network") {			//CLIENT
//		
//		array_splice($menuItemList,6,1);			//hide job allocation
//		array_splice($menuItemList[1]["nextLevel"],1,1);	//hide clients
//		array_splice($menuItemList[1]["nextLevel"],3,1);	//hide service providers
//		array_splice($menuItemList[2]["nextLevel"],0,1);	//hide manufacturers
//		array_splice($menuItemList[2]["nextLevel"],2,1);	//hide pricing structure
//		
//		if($this->user->UserType!="Client") {			//BRANCH
//		
//		    array_splice($menuItemList,1,1);			//hide organisation setup
//		    array_splice($menuItemList[1]["nextLevel"],0,1);	//hide unit types
//		    array_splice($menuItemList[1]["nextLevel"],2,2);	//hide product numbers and client service types
//		    
//		}
//		
//	    }
//	    
//	}
//        echo"<pre>";
//        print_r($menuItemList[3]);
//        echo"</pre>";
        ///setting visible items for permission 8001 

        if (array_key_exists("AP8001", $this->user->Permissions)) {
            //company setup
            $menuItemList2[0]['title'] = $menuItemList[3]['title'];
            $menuItemList2[0]['code'] = $menuItemList[3]['code'];
            $menuItemList2[0]['link'] = $menuItemList[3]['link'];
            $menuItemList2[0]['tooltip'] = $menuItemList[3]['tooltip'];
            $menuItemList2[0]['shortDesc'] = $menuItemList[3]['shortDesc'];
            $menuItemList2[0]['description'] = $menuItemList[3]['description'];
            $menuItemList2[0]['nextLevel'][] = $menuItemList[3]['nextLevel'][7];
            // $menuItemList2[0]['nextLevel'][]=$menuItemList[3]['nextLevel'][5];
            //product setup
            $menuItemList2[1]['title'] = $menuItemList[4]['title'];
            $menuItemList2[1]['code'] = $menuItemList[4]['code'];
            $menuItemList2[1]['link'] = $menuItemList[4]['link'];
            $menuItemList2[1]['tooltip'] = $menuItemList[4]['tooltip'];
            $menuItemList2[1]['shortDesc'] = $menuItemList[4]['shortDesc'];
            $menuItemList2[1]['description'] = $menuItemList[4]['description'];
            $menuItemList2[1]['nextLevel'][] = $menuItemList[4]['nextLevel'][0];
            $menuItemList2[1]['nextLevel'][] = $menuItemList[4]['nextLevel'][2];

            //lookup
            $menuItemList2[2]['title'] = $menuItemList[1]['title'];
            $menuItemList2[2]['code'] = $menuItemList[1]['code'];
            $menuItemList2[2]['link'] = $menuItemList[1]['link'];
            $menuItemList2[2]['tooltip'] = $menuItemList[1]['tooltip'];
            $menuItemList2[2]['shortDesc'] = $menuItemList[1]['shortDesc'];
            $menuItemList2[2]['description'] = $menuItemList[1]['description'];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][16];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][17];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][18];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][19];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][23];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][24];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][25];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][26];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][27];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][28];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][29];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][30];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][31];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][32];
            $menuItemList2[2]['nextLevel'][] = $menuItemList[1]['nextLevel'][33];



            $menuItemList = $menuItemList2;
        }


        //$this->log("MENU LIST: " . var_export($menuItemList,true));

        /*
          $menuItemListNew = array();

          for($i=0; $i<count($menuItemList); $i++) {
          if(isset($menuItemList[$i]['accessPermission']) && !$this->user->SuperAdmin) {
          if(isset($this->user->Permissions[$menuItemList[$i]['accessPermission']])) {
          $menuItemListNew[] = $menuItemList[$i];
          }
          } else {
          $menuItemListNew[] = $menuItemList[$i];
          }
          }

          $displayMenuItemList = $menuItemListNew;
         */

        $displayMenuItemList = $menuItemList;

        $levelTwoPage = isset($args[0]) ? $args[0] : '';
        $levelTwoPageName = $levelTwoPageDesc = '';
        if ($levelTwoPage) {
            for ($i = 0; $i < count($menuItemList); $i++) {
                if ($menuItemList[$i]['code'] == $levelTwoPage) {
                    $levelTwoPageName = $menuItemList[$i]['title'];
                    $levelTwoPageDesc = $menuItemList[$i]['description'];
                    $displayMenuItemList = $menuItemList[$i]['nextLevel'];
                    break;
                }
            }
        }

        $sorter = array();
        $ret = array();

        reset($displayMenuItemList);

        foreach ($displayMenuItemList as $ii => $va) {
            $sorter[$ii] = $va['title'];
        }

        asort($sorter);

        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $displayMenuItemList[$ii];
        }

        $displayMenuItemList = $ret;

        $this->smarty->assign('page', $this->page);
        $this->smarty->assign('levelTwoPage', $levelTwoPage);
        $this->smarty->assign('levelTwoPageName', $levelTwoPageName);
        $this->smarty->assign('levelTwoPageDesc', $levelTwoPageDesc);
        $this->smarty->assign('displayMenuItemList', $displayMenuItemList);
        $this->smarty->display('SystemAdmin.tpl');

        return;
    }

    //Alternative field names facility
    //2012-10-15 © Vic <v.rutkunas@pccsuk.com>

    public function alternativeFieldNamesAction($args) {

        $functionAction = isset($args[0]) ? $args[0] : '';
        $selectedRowId = isset($args[1]) ? $args[1] : '';

        $Skyline = $this->loadModel('Skyline');

        $this->smarty->assign('statuses', $this->statuses);
        $this->smarty->assign('SupderAdmin', $this->user->SuperAdmin);
        $this->smarty->assign('userPermissions', $this->user->Permissions);
        //$this->smarty->assign('userBrands', $this->user->Brands);
        $this->smarty->assign('accessDeniedMsg', $this->messages->getError(1023, 'default', $this->lang));

        $accessErrorFlag = false;

        $this->page = $this->messages->getPage('alternativeFieldNames', $this->lang);
        
                    //Getting Page title & Description from database
            $model = $this->loadModel('GroupHeadings');        
            $groupHedingResults = $model->getHeadingPageDetails('alternativeFieldNames');

            // Over write the page title & description
            $page['Text']['page_title'] = isset($groupHedingResults[0])?$groupHedingResults[0]:$page['Text']['page_title'];
            $page['Text']['description'] = isset($groupHedingResults[1])?$groupHedingResults[1]:$page['Text']['description'];

        
        $this->smarty->assign('page', $this->page);

        switch ($functionAction) {

            case 'insert': {

                    $datarow = ['alternativeFieldID' => '',
                        'primaryFieldID' => '',
                        'alternativeFieldName' => '',
                        'status' => 'Active',
                        'brand' => '',
                        'brandID' => ''
                    ];
                    $this->smarty->assign('datarow', $datarow);

                    $model = $this->loadModel('AlternativeFieldNames');
                    $fields = $model->getAlternativeFields();
                    $this->smarty->assign("fields", $fields);

                    $brand_model = $this->loadModel('Brands');
                    if ($this->user->Username == "sa") {
                        $brands = $brand_model->getAllBrands();
                    }
                    if ($this->user->UserType == "Network") {
                        $brands = $brand_model->getNetworkBrands($this->user->NetworkID);
                    }
                    if ($this->user->UserType == "Client") {
                        $brands = $brand_model->getClientBrands($this->user->ClientID);
                    }
                    $this->smarty->assign("brands", $brands);

                    //Checking user permissons to display the page.
                    /*
                      if($this->user->SuperAdmin) {
                      $accessErrorFlag = false;
                      } elseif (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                      if($datarow['PrimaryFieldName']=='') {
                      $accessErrorFlag = true;
                      }
                      }
                     */

                    $this->smarty->assign('form_legend', $this->page['Text']['insert_page_legend']);
                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                    $htmlCode = $this->smarty->fetch('alternativeFieldNamesForm.tpl');

                    echo $htmlCode;

                    break;
                }


            case 'update': {

                    $args["alternativeFieldID"] = $selectedRowId;

                    $model = $this->loadModel('AlternativeFieldNames');

                    $datarow = $model->fetchRow($args);

                    //echo("<pre>"); print_r($datarow); echo("</pre>");

                    $this->smarty->assign('form_legend', $this->page['Text']['update_page_legend']);

                    $this->smarty->assign('datarow', $datarow);

                    //Checking user permissons to display the page.
                    if ($this->user->SuperAdmin) {
                        $accessErrorFlag = false;
                    } elseif (isset($this->user->Permissions['AP7001']) || isset($this->user->Permissions['AP7002'])) {
                        if (!isset($this->user->Brands[$datarow['brandID']])) {
                            $accessErrorFlag = true;
                        }
                    }

                    $this->smarty->assign('accessErrorFlag', $accessErrorFlag);

                    $htmlCode = $this->smarty->fetch('alternativeFieldNamesForm.tpl');

                    echo $htmlCode;

                    break;
                }

            default: {
                    $this->smarty->display('alternativeFieldNames.tpl');
                }
        }
    }

    public function deleteAltFieldAction() {

        $id = $_POST["id"];
        $sys_users_model = $this->loadModel("SystemUsers");
        $result = $sys_users_model->deleteAltField($id);
    }

    //Alternative field names facility
    //2012-10-15 © Vic <v.rutkunas@pccsuk.com>

    public function getAltNamesAction() {

        $model = $this->loadModel('AlternativeFieldNames');

        $allFields = $model->getAlternativeFields();
        $brandFields = $model->getBrandAlternativeFields($_POST["brandID"]);

        $all = array();
        $brand = array();

        foreach ($allFields as $field) {
            $all[] = $field["primaryFieldID"];
        }

        foreach ($brandFields as $field) {
            $brand[] = $field["primaryFieldID"];
        }

        $result = array();

        foreach ($allFields as $field) {
            if (!in_array($field["primaryFieldID"], $brand)) {
                $result[] = $field;
            }
        }

        echo json_encode($result);
    }

    //this function is used to create SP specific records by linking them to skyline main tables (eg. Manufacturer -> ServiceProviderManufacturer)
    //Andris Polnikovs a.polnikovs@pccsuk.com
    public function CopyFromMainAction($args) {
        $table = $args[0]; //main table
        switch ($table) {
            case "Manufacturer": {
                    $model = "Manufacturers";
                    break;
                }
            case "Model": {
                    $model = "Models";
                    break;
                }
            case "Supplier": {
                    $model = "Suppliers";
                    break;
                }
        }
        $model = $this->loadModel($model);
        $model->CopyFromMain($_POST['ids'], $this->user->ServiceProviderID);
    }

}

?>
