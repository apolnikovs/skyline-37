<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of JobFaultCodes Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     04/06/2013
 */
class JobFaultCodes extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->table = "job_fault_code";
        $this->fields = [
            "JobFaultCodeID",
            "JobFaultCode",
            "FieldNo",
            "FieldType",
            "NonFaultCode",
            "MainInFault",
            "MainOutFault",
            "Status",
	    "ApproveStatus"
        ];
    }
    public function fetch($args) {	
        $spId = isset($args['firstArg'])?$args['firstArg']:'';
	$dataStatus = isset($args['secondArg'])?$args['secondArg']:'';
	
        $args['where']    = " isDeleted='No' ";
	/* Updated By Praveen Kumar N : Show Inactive Records [START] */
	if($dataStatus != "" && $dataStatus == 'In-active') {
            $args['where'].= " AND Status='".$dataStatus."' ";
	}
	if($dataStatus != "" && $dataStatus == 'Pending') {
            $args['where'].= " AND ApproveStatus='".$dataStatus."' ";
	}
	/* [END] */
        if($spId != "")
            $args['where'].= " AND ServiceProviderID='".$spId."' ";
        $output = $this->ServeDataTables($this->conn, $this->table, $this->fields, $args);
	return $output;
    }
    
    public function processData($args) {
	if(empty($args['JobFaultCodeID'])) {
	    return $this->create($args);
        } else {
            return $this->update($args);
        }
    }
    
    public function create($args) {
	
	$q="INSERT INTO job_fault_code (ServiceProviderID, RepairTypeID, JobFaultCode, FieldNo, FieldType, Lookup, NonFaultCode, MainInFault, MainOutFault, UseAsGenericFaultDescription, RestrictLength, MinLength, MaxLength, ForceFormat, RequiredFormat, ReplicateToFaultDescription, FillFromDOP, 
        HideWhenRelatedCodeIsBlank, FaultCodeNo, HideForThirdPartyJobs, NotReqForThirdPartyJobs, ReqAtBookingForChargeableJobs, ReqAtCompletionForChargeableJobs, ReqAtBookingForWarrantyJobs, ReqAtCompletionForWarrantyJobs, ReqIfExchangeIssued, ReqOnlyForRepairType, UseLookup, ForceLookup, Status, ApproveStatus, CreatedDate, CreatedUserID)
	VALUES (:ServiceProviderID, :RepairTypeID, :JobFaultCode, :FieldNo, :FieldType, :Lookup, :NonFaultCode, :MainInFault, :MainOutFault, :UseAsGenericFaultDescription, :RestrictLength, :MinLength, :MaxLength, :ForceFormat, :RequiredFormat, :ReplicateToFaultDescription, :FillFromDOP, 
        :HideWhenRelatedCodeIsBlank, :FaultCodeNo, :HideForThirdPartyJobs, :NotReqForThirdPartyJobs, :ReqAtBookingForChargeableJobs, :ReqAtCompletionForChargeableJobs, :ReqAtBookingForWarrantyJobs, :ReqAtCompletionForWarrantyJobs, :ReqIfExchangeIssued, :ReqOnlyForRepairType, :UseLookup, :ForceLookup, :Status, :ApproveStatus, :CreatedDate, :CreatedUserID)";
        
        $values = array("ServiceProviderID"=>$args['ServiceProviderID'], "RepairTypeID"=>$args['RepairTypeID'], "JobFaultCode"=>$args['JobFaultCode'], "FieldNo"=>$args['FieldNo'],  "FieldType"=>(isset($args['FieldType'])?$args['FieldType']:""),
        "Lookup"=>(!isset($args['Lookup'])?'No':'Yes'), "NonFaultCode"=>(!isset($args['NonFaultCode'])?'No':'Yes'), "MainInFault"=>(!isset($args['MainInFault'])?'No':'Yes'), "MainOutFault"=>(!isset($args['MainOutFault'])?'No':'Yes'), "UseAsGenericFaultDescription"=>(!isset($args['UseAsGenericFaultDescription'])?'No':'Yes'), "RestrictLength"=>(!isset($args['RestrictLength'])?'No':'Yes'), 
        "MinLength"=>$args['MinLength'], "MaxLength"=>$args['MaxLength'], "ForceFormat"=>(!isset($args['ForceFormat'])?'No':'Yes'), "RequiredFormat"=>(!isset($args['RequiredFormat'])?'No':'Yes'), "ReplicateToFaultDescription"=>(!isset($args['ReplicateToFaultDescription'])?'No':'Yes'), "FillFromDOP"=>(!isset($args['FillFromDOP'])?'No':'Yes'), "HideWhenRelatedCodeIsBlank"=>(!isset($args['HideWhenRelatedCodeIsBlank'])?'No':'Yes'), 
        "FaultCodeNo"=>$args['FaultCodeNo'], "HideForThirdPartyJobs"=>(!isset($args['HideForThirdPartyJobs'])?'No':'Yes'), "NotReqForThirdPartyJobs"=>(!isset($args['NotReqForThirdPartyJobs'])?'No':'Yes'), "ReqAtBookingForChargeableJobs"=>(!isset($args['ReqAtBookingForChargeableJobs'])?'No':'Yes'), "ReqAtCompletionForChargeableJobs"=>(!isset($args['ReqAtCompletionForChargeableJobs'])?'No':'Yes'), 
        "ReqAtBookingForWarrantyJobs"=>(!isset($args['ReqAtBookingForWarrantyJobs'])?'No':'Yes'), "ReqAtCompletionForWarrantyJobs"=>(!isset($args['ReqAtCompletionForWarrantyJobs'])?'No':'Yes'), "ReqIfExchangeIssued"=>(!isset($args['ReqIfExchangeIssued'])?'No':'Yes'), "ReqOnlyForRepairType"=>(!isset($args['ReqOnlyForRepairType'])?'No':'Yes'), "UseLookup"=>(!isset($args['UseLookup'])?'No':'Yes'), "ForceLookup"=>(!isset($args['ForceLookup'])?'No':'Yes'), 
        "Status"=>(!isset($args['Status'])?'Active':'In-active'), "ApproveStatus"=>(!isset($args['ApproveStatus'])?'Approved':'Pending'), "CreatedDate"=>date("Y-m-d H:i:s"),"CreatedUserID"=>$this->controller->user->UserID);
	$query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been inserted successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function fetchRow($args) {
        $sql = 'SELECT * FROM job_fault_code WHERE JobFaultCodeID=:JobFaultCodeID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':JobFaultCodeID' => $args['JobFaultCodeID']));
        $result = $fetchQuery->fetch();
        return $result;
    }
    
    public function update($args) {
        
        
	$q="UPDATE job_fault_code SET ServiceProviderID=:ServiceProviderID, RepairTypeID=:RepairTypeID, JobFaultCode=:JobFaultCode, FieldNo=:FieldNo, FieldType=:FieldType, Lookup=:Lookup, NonFaultCode=:NonFaultCode, MainInFault=:MainInFault, MainOutFault=:MainOutFault, UseAsGenericFaultDescription=:UseAsGenericFaultDescription, RestrictLength=:RestrictLength, MinLength=:MinLength, MaxLength=:MaxLength, ForceFormat=:ForceFormat, RequiredFormat=:RequiredFormat, 
            ReplicateToFaultDescription=:ReplicateToFaultDescription, FillFromDOP=:FillFromDOP, HideWhenRelatedCodeIsBlank=:HideWhenRelatedCodeIsBlank, FaultCodeNo=:FaultCodeNo, HideForThirdPartyJobs=:HideForThirdPartyJobs, NotReqForThirdPartyJobs=:NotReqForThirdPartyJobs, ReqAtBookingForChargeableJobs=:ReqAtBookingForChargeableJobs, ReqAtCompletionForChargeableJobs=:ReqAtCompletionForChargeableJobs, ReqAtBookingForWarrantyJobs=:ReqAtBookingForWarrantyJobs, 
            ReqAtCompletionForWarrantyJobs=:ReqAtCompletionForWarrantyJobs, ReqIfExchangeIssued=:ReqIfExchangeIssued, ReqOnlyForRepairType=:ReqOnlyForRepairType, UseLookup=:UseLookup, ForceLookup=:ForceLookup, Status=:Status,ApproveStatus=:ApproveStatus, ModifiedDate=:ModifiedDate, ModifiedUserID=:ModifiedUserID WHERE JobFaultCodeID=:JobFaultCodeID";
        
        $values = array("ServiceProviderID"=>$args['ServiceProviderID'], "RepairTypeID"=>$args['RepairTypeID'], "JobFaultCode"=>$args['JobFaultCode'], "FieldNo"=>$args['FieldNo'],  "FieldType"=>(isset($args['FieldType'])?$args['FieldType']:""),
        "Lookup"=>(!isset($args['Lookup'])?'No':'Yes'), "NonFaultCode"=>(!isset($args['NonFaultCode'])?'No':'Yes'), "MainInFault"=>(!isset($args['MainInFault'])?'No':'Yes'), "MainOutFault"=>(!isset($args['MainOutFault'])?'No':'Yes'), "UseAsGenericFaultDescription"=>(!isset($args['UseAsGenericFaultDescription'])?'No':'Yes'), "RestrictLength"=>(!isset($args['RestrictLength'])?'No':'Yes'), 
        "MinLength"=>$args['MinLength'], "MaxLength"=>$args['MaxLength'], "ForceFormat"=>(!isset($args['ForceFormat'])?'No':'Yes'), "RequiredFormat"=>(!isset($args['RequiredFormat'])?'No':'Yes'), "ReplicateToFaultDescription"=>(!isset($args['ReplicateToFaultDescription'])?'No':'Yes'), "FillFromDOP"=>(!isset($args['FillFromDOP'])?'No':'Yes'), "HideWhenRelatedCodeIsBlank"=>(!isset($args['HideWhenRelatedCodeIsBlank'])?'No':'Yes'), 
        "FaultCodeNo"=>$args['FaultCodeNo'], "HideForThirdPartyJobs"=>(!isset($args['HideForThirdPartyJobs'])?'No':'Yes'), "NotReqForThirdPartyJobs"=>(!isset($args['NotReqForThirdPartyJobs'])?'No':'Yes'), "ReqAtBookingForChargeableJobs"=>(!isset($args['ReqAtBookingForChargeableJobs'])?'No':'Yes'), "ReqAtCompletionForChargeableJobs"=>(!isset($args['ReqAtCompletionForChargeableJobs'])?'No':'Yes'), 
        "ReqAtBookingForWarrantyJobs"=>(!isset($args['ReqAtBookingForWarrantyJobs'])?'No':'Yes'), "ReqAtCompletionForWarrantyJobs"=>(!isset($args['ReqAtCompletionForWarrantyJobs'])?'No':'Yes'), "ReqIfExchangeIssued"=>(!isset($args['ReqIfExchangeIssued'])?'No':'Yes'), "ReqOnlyForRepairType"=>(!isset($args['ReqOnlyForRepairType'])?'No':'Yes'), "UseLookup"=>(!isset($args['UseLookup'])?'No':'Yes'), "ForceLookup"=>(!isset($args['ForceLookup'])?'No':'Yes'), 
        "Status"=>(!isset($args['Status'])?'Active':'In-active'), "ApproveStatus"=>(!isset($args['ApproveStatus'])?'Approved':'Pending'), "ModifiedDate"=>date("Y-m-d H:i:s"),"ModifiedUserID"=>$this->controller->user->UserID, "JobFaultCodeID"=>$args['JobFaultCodeID']);
	$query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);	
	if($query) {
        //$value = array("JobFaultCodeLookup"=>$args['JobFaultCodeLookup']);
            $lookupValues = $args['JobFaultCodeLookup'];
        foreach($lookupValues AS $value){
        $lID = $args['JobFaultCodeID'];
        $sql = "insert into job_fault_code_lookup_to_job_fault_code  (JobFaultCodeID,JobFaultCodeLookupID) values ($lID,$value)";
        $this->execute($this->conn, $sql);
        }
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been updated successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function deleteJobFaultCode($id) {
        $q = "update job_fault_code set isDeleted='Yes' where JobFaultCodeID=:JobFaultCodeID";
	$values = array("JobFaultCodeID" => $id);
        $query = $this->conn->prepare($q, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
	$query->execute($values);
	if($query) {
	    return array(   'status' => 'OK',
			    'message' => 'Your data has been deleted successfully.');
	} else {
            return array(   'status' => 'ERROR',
			    'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
	}
    }
    
    public function insertJobFaultCodes($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
	$P["ApproveStatus"] = isset($P["ApproveStatus"]) ? $P["ApproveStatus"] : "Approved";
        $id = $this->SQLGen->dbInsert('job_fault_code', $this->fields, $P, true, true);
        return $id;
    }

    public function updateJobFaultCodes($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
	$P["ApproveStatus"] = isset($P["ApproveStatus"]) ? $P["ApproveStatus"] : "Approved";
        $id = $this->SQLGen->dbUpdate('job_fault_code', $this->fields, $P, "JobFaultCodeID=" . $P['JobFaultCodeID'], true);
    }

    public function getJobFaultCodesData($id) {
        $sql = "select * from job_fault_code where JobFaultCodeID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

    public function deleteJobFaultCodes($id) {
        $sql = "update job_fault_code set Status='In-Active' where JobFaultCodeID=$id";
        $this->execute($this->conn, $sql);
    }

    ////job_fault_code functions 

    public function checkIfOrderSection($id) {
        $sql = "select DisplayOrderSection from job_fault_code where PartStatusID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0]['DisplayOrderSection'];
    }

    public function addLinkedItem($p, $c) {
        $u = $this->controller->user->UserID;
        $sql = "insert into job_fault_code_lookup_to_job_fault_code  (JobFaultCodeID,JobFaultCodeLookupID,ModifiedUserID) values ($p,$c,$u)";
        $this->execute($this->conn, $sql);
    }

    public function delLinkedItem($p, $c) {
        $sql = "delete from job_fault_code_lookup_to_job_fault_code where JobFaultCodeID=$p and JobFaultCodeLookupID =$c";
        $this->execute($this->conn, $sql);
    }

    public function loadLinkedItem($p) {
        $u = $this->controller->user->UserID;
        if ($this->controller->user->SuperAdmin == 1) {
            $t = "job_fault_code_lookup";
            $m = "JobFaultCodeLookupID";
        } else {
            $t = "job_fault_code_lookup";
            $m = "JobFaultCodeLookupID";
        }
        $sql = "select am.JobFaultCodeLookupID, 
            LookupName as `ItemName`  from job_fault_code_lookup_to_job_fault_code am
                join $t m on m.$m=am.JobFaultCodeLookupID

            where JobFaultCodeID=$p";
        return $this->Query($this->conn, $sql);
    }

}

?>